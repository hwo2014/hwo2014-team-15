/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package noobbot.util;

import java.util.HashMap;
import java.util.List;
import noobbot.graph.Edge;
import noobbot.graph.Edge.Direction;
import noobbot.graph.Node;

/**
 *
 * @author teturun
 */
public class PathToDirectionsConverter {
    
    public enum Command {
        NO_ACTION,
        TURN_LEFT,
        TURN_RIGHT
    }
    
    public static HashMap<Integer, Command> getDirections(List<Node> path) {
        
        HashMap<Integer, Command> commands = new HashMap<>();
        
        for (int i = 0; i < path.size(); i++) {
            
            Node curr = path.get(i);
            Node next = null;
            Node nextNext = null;
            if (i + 2 < path.size()) {
                
                next = path.get(i+1);
                nextNext = path.get(i+2);
                
            }
                            
            if (next != null && nextNext != null && next.getTrackPieceId() == nextNext.getTrackPieceId() && next.getLaneId() != nextNext.getLaneId() && next.hasSwitch()) {
                HashMap<Direction, Edge> edges = next.getEdges();

                if (edges.containsKey(Direction.LEFT_TURN) && edges.get(Direction.LEFT_TURN).getTo().equals(nextNext)) {
                    commands.put(curr.getTrackPieceId(), Command.TURN_LEFT);
                }
                else if (edges.containsKey(Direction.RIGHT_TURN) && edges.get(Direction.RIGHT_TURN).getTo().equals(nextNext)) {
                    commands.put(curr.getTrackPieceId(), Command.TURN_RIGHT);
                }
            }
            else if (next != null && curr.getLaneId() == next.getLaneId()) {
                commands.put(curr.getTrackPieceId(), Command.NO_ACTION);
            }
            else {
                commands.put(curr.getTrackPieceId(), Command.NO_ACTION);
            }
            
        }
        
        return commands;
    }
    
}
