package noobbot;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;

import noobbot.analyzer.CarAnalyzer;
import noobbot.analyzer.RaceContainer;
import noobbot.analyzer.message.DriveAction;
import noobbot.analyzer.message.PaceNote;
import noobbot.analyzer.state.CarState;
import noobbot.analyzer.waypoints.Waypoint;
import noobbot.driver.CoDriver;
import noobbot.driver.Driver;
import noobbot.events.CarEnteredTrackPieceEvent;
import noobbot.events.CarFinishedLapEvent;
import noobbot.events.CarSwitchedLaneEvent;
import noobbot.events.MessageHandler;
import noobbot.events.MessageListener;
import noobbot.events.TrackListener;
import noobbot.events.TrackObserver;
import noobbot.objects.CarPosition;
import noobbot.objects.Race;
import noobbot.objects.Turbo;
import noobbot.planning.PathCalculationService;

import com.google.gson.Gson;
import noobbot.objects.CarPositions;
import noobbot.objects.Id;


public class Main implements TrackListener, MessageListener {

	private RaceContainer raceContainer;
	private CarAnalyzer carAnalyzer;
    
	private Driver driver;
	private CoDriver coDriver;

	private MessageHandler msgHandler;
	private TrackObserver observer;
	
	private PathCalculationService pathCalculationService;

    private String botName;
    
    final Gson gson = new Gson();
    private PrintWriter writer;

    
    public static void main(String... args) throws IOException {
        String host = args[0];
        int port = Integer.parseInt(args[1]);
        String botName = args[2];
        String botKey = args[3];

        System.out.println("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

        final Socket socket = new Socket(host, port);
        final PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));

        final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));

        new Main(reader, writer, botName, botKey);

    }

    private void init(String botName) {
        this.botName = botName;
        this.raceContainer = new RaceContainer(botName);
        this.carAnalyzer = new CarAnalyzer(raceContainer);
        
        this.driver = new Driver();
        this.coDriver = new CoDriver(raceContainer);

        this.msgHandler = new MessageHandler();
        this.observer = new TrackObserver(raceContainer);
    	
        this.pathCalculationService = new PathCalculationService();
    }
    
    public Main(final BufferedReader reader, final PrintWriter writer, final String botName, final String botKey) throws IOException {
        this.writer = writer;

        init(botName);
        
        send(new Join(botName, botKey));

        handleRace(reader, botName, botKey);
    }

    public Main(final String host, final int port, final String botName, final String botKey, final String trackName, final String password, final int nrOfCars, final boolean join) throws IOException {

        final Socket socket = new Socket(host, port);
        this.writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));
        final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));

        init(botName);
        
        if (join) {
            send(new JoinRace(new BotId(botName, botKey), trackName, password, nrOfCars));
        } else {
            send(new CreateRace(new BotId(botName, botKey), trackName, password, nrOfCars));
        }

        handleRace(reader, botName, botKey);
    }

    private void handleRace(final BufferedReader reader, final String botName, final String botKey) throws IOException {
        
        Configuration.useAnalyzer = true;
        
        this.raceContainer.updateCarState(carAnalyzer.initialCarState());
        this.raceContainer.setRaceStarted(false);

        this.observer.addTrackListener(this);
        this.observer.addTrackListener(coDriver);
        this.msgHandler.addListener(this);
        
        this.msgHandler.startListening(reader);
        
    }
 
    private void send(final SendMsg msg) {
       	System.out.println("[" + botName + "]: Send: " + msg.toJson());
        
        this.writer.println(msg.toJson());
        this.writer.flush();
    }
    
    private void send(final SendTickMsg msg) {
       	System.out.println("[" + botName + "]: Send tick msg: " + msg.toJson());
        
        this.writer.println(msg.toJson());
        this.writer.flush();
    }

    @Override
    public void carEnteredTrackPiece(CarEnteredTrackPieceEvent e) {
        System.out.println("[" + botName + "]: " + "carEntered: " + e.toString());
    }

    @Override
    public void carSwitchedLanes(CarSwitchedLaneEvent e) {
        System.out.println("[" + botName + "]: " + "carSwitchedLanes: " + e.toString());
    }
    
    @Override
    public void carFinishedLap(CarFinishedLapEvent e) {
        
    }

    @Override
    public void carPositionsReceived(CarPositions carPositions) {
        
//        System.out.println("[" + botName + "]: Car positions" + ", data=" + carPositions);
        
//        if (raceContainer.isRaceStarted()) {
            
            int gameTick = carPositions.gameTick;
            System.out.println("Positions received... TICK=" + gameTick);
                
            if (Configuration.useAnalyzer) {
                
                
                for (CarPosition pos : carPositions.positions) {
                    raceContainer.updateCarPosition(pos);
                }
                observer.tick();

            	if (raceContainer.isCurrentlyCrashed()) {
            		send(new Throttle(0.5, gameTick));
            	} else {                	

                    CarState lastCarState = raceContainer.getLatestState(botName);
                    
                    System.out.println("[" + botName + "]: lastCarState=" + lastCarState);
                	// update last waypoint with last state (current state after crashing has undesired data)
                    raceContainer.getWaypoints().recalculateWaypoint(raceContainer.getWaypoints().getPreviousWaypoint(lastCarState.pointOnTrack), lastCarState, raceContainer.getSpeedChangeStates());

                    CarPosition carPos = raceContainer.getLatestPosition(botName);
                    System.out.println("[" + botName + "]: carPos=" + carPos);
                    PaceNote pacenote = coDriver.tellPaceNote();
                    System.out.println("[" + botName + "]: paceNote=" + pacenote);
                    CarState state = carAnalyzer.calculateCarState(lastCarState, carPos);
                    System.out.println("[" + botName + "]: carState=" + state);
                    Waypoint nextWaypoint = raceContainer.getWaypoints().getNextWaypoint(state.pointOnTrack);
                    System.out.println("[" + botName + "]: nextWaypoint=" + nextWaypoint);
                    state.distanceToNextWaypoint = carAnalyzer.getCalculator().calculateDistance(state.pointOnTrack, nextWaypoint.pointOnTrack);
                    System.out.println("[" + botName + "]: distanceToNextWaypoint=" + state.distanceToNextWaypoint);
                    raceContainer.getSpeedChangeStates().updateWith(lastCarState, raceContainer.currentPiece(carPos));
                    
                    DriveAction driveAction = driver.drive(pacenote, state, nextWaypoint);

                    if (driveAction.type == DriveAction.Type.THROTTLE) 
                    {
                        System.out.println("THROTTLE TIME, TICK=" + gameTick);
                    	double throttle = (Double) driveAction.content;

                    	send(new Throttle(throttle, gameTick));   

                    	state = carAnalyzer.appendNewThrottle(lastCarState, state, throttle);
                        System.out.println("[" + botName + "]: state after appendNewThrottle=" + state);
                        raceContainer.updateCarState(state);
                    }
                    else if (driveAction.type == DriveAction.Type.SWITCH) 
                    {
                    	String direction = (String) driveAction.content;

                    	send(new Switch(direction, gameTick));
                    }
                    else if (driveAction.type == DriveAction.Type.TURBO) 
                    {
                    	String message = (String) driveAction.content;

                    	send(new TurboMsg(message, gameTick));
                    }
                    
//                    System.out.println("[" + botName + "]: " + "state: " + state);
//                    if (state.acceleration > 0.1) 
//                    	System.out.println("[" + botName + "]: " + "ACC  state: " + state.accelerationState());
                    if (state.deceleration > 0.1) 
                    	System.out.println("[" + botName + "]: " + "DEC  state: " + state.decelerationState());
                    if (state.centripetalForce > 0.1 || state.slipAngle > 0.1) 
                    	System.out.println("[" + botName + "]: " + "SIDE state: " + state.sideSlipState());

                } // not crashed

            }
            else {
                // default action from example code
                send(new Throttle(0.5, gameTick));
            }
//        }

    }

    @Override
    public void joinReceived(Object data) {
        System.out.println("[" + botName + "]: Joined");
    }

    @Override
    public void gameInitReceived(Race race) {
        
        System.out.println("[" + botName + "]: Race init");
        System.out.println("[" + botName + "]: " + race);
        
        raceContainer.setrace(race);
    }

    @Override
    public void gameEndReceived(Object data) { // Warning: game end message is received when any car finishes
        System.out.println("[" + botName + "]: Race end" + ", data=" + data);
        
    }

    @Override
    public void dnfReceived(Object data) {
        System.out.println("[" + botName + "]: disqualified" + ", data=" + data);
    }

    @Override
    public void yourCarReceived(Object data) {
         System.out.println("[" + botName + "]: My car info" + ", data=" + data);
    }

    @Override
    public void lapFinishedReceived(Object data) {
        System.out.println("[" + botName + "]: Lap finished" + ", data=" + data);
        raceContainer.lapFinish(); // Warning: lap finished message is received when any car finishes
    }

    @Override
    public void finishReceived(Id carId) {
        System.out.println("[" + botName + "]: Finish" + ", " + carId);
    }

    @Override
    public void tournamentEndReceived(Object data) {
        System.out.println("[" + botName + "]: Tournament end" + ", data=" + data);
    }

    @Override
    public void crashReceived(Id carId) {
        System.out.println("[" + botName + "]: ################# CRASH CRASH CRASH ################# " + ", " + carId);
        if (carId.name.equals(botName)) {
            raceContainer.crash();
        }
    }

    @Override
    public void spawnReceived(Id carId) {
        System.out.println("[" + botName + "]: Spawn" + ", " + carId);
        if (carId.name.equals(botName)) {
            raceContainer.spawn();
        }
    }

    @Override
    public void unidentifiedMessageReceived(MsgWrapper message) {
        System.out.println("[" + botName + "]: UNHANDLED msgFromServer.msgType " + message.msgType + ", data=" + message.data); 
        send(new Ping());
    }

    @Override
    public void gameStartReceived(Object data) {
        System.out.println("[" + botName + "]: Race start" + ", data=" + data);
        raceContainer.setRaceStarted(true);
        send(new Ping());
    }

    @Override
    public void turboAvailableReceived(Turbo turbo) {
        System.out.println("[" + botName + "]: Turbo available" + ", " + turbo);
        raceContainer.setTurbo(turbo);
    }

}

abstract class SendMsg {

    public String toJson() {
        return new Gson().toJson(new MsgWrapper(this));
    }

    protected Object msgData() {
        return this;
    }

    protected abstract String msgType();
}

abstract class SendTickMsg extends SendMsg {
    
    @Override
    public String toJson() {
        return new Gson().toJson(new TickMsgWrapper(this));
    }
    
    protected abstract int gameTick();
    
}

class Join extends SendMsg {

    public final String name;
    public final String key;

    Join(final String name, final String key) {
        this.name = name;
        this.key = key;
    }

    @Override
    protected String msgType() {
        return "join";
    }
}

class CreateRace extends SendMsg {

    public final BotId botId;
    public final String trackName;
    public final String password;
    public final int carCount;

    CreateRace(final BotId botId, final String trackName, final String password, final int carCount) {
        this.botId = botId;
        this.trackName = trackName;
        this.password = password;
        this.carCount = carCount;
    }

    @Override
    protected String msgType() {
        return "createRace";
    }
}

class BotId {

    public final String name;
    public final String key;

    BotId(final String name, final String key) {
        this.name = name;
        this.key = key;
    }
}

class JoinRace extends SendMsg {

    public final BotId botId;
    public final String trackName;
    public final String password;
    public final int carCount;

    JoinRace(final BotId botId, final String trackName, final String password, final int carCount) {
        this.botId = botId;
        this.trackName = trackName;
        this.password = password;
        this.carCount = carCount;
    }

    @Override
    protected String msgType() {
        return "joinRace";
    }
}

class Ping extends SendMsg {

    @Override
    protected String msgType() {
        return "ping";
    }
}

class Throttle extends SendTickMsg {

    private double value;
    private int gameTick;

    public Throttle(double value, int gameTick) {
        this.value = value;
        this.gameTick = gameTick;
    }

    @Override
    protected Object msgData() {
        return value;
    }

    @Override
    protected String msgType() {
        return "throttle";
    }
    
    @Override
    protected int gameTick() {
        return gameTick;
    }

}

class Switch extends SendTickMsg {

    private String value;
    private int gameTick;

    public Switch(String value, int gameTick) {
        this.value = value;
        this.gameTick = gameTick;
    }

    @Override
    protected Object msgData() {
        return value;
    }

    @Override
    protected String msgType() {
        return "switchLane";
    }
    
    @Override
    protected int gameTick() {
        return gameTick;
    }
}

class TurboMsg extends SendTickMsg {

    private String value;
    private int gameTick;

    public TurboMsg(String value, int gameTick) {
        this.value = value;
        this.gameTick = gameTick;
    }

    @Override
    protected Object msgData() {
        return value;
    }

    @Override
    protected String msgType() {
        return "turbo";
    }
    
    @Override
    protected int gameTick() {
        return gameTick;
    }
}

