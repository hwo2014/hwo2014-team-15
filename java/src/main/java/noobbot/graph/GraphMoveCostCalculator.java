/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package noobbot.graph;

import com.louhigames.graph.algorithms.pathfinding.PathFindingObject;

/**
 *
 * @author teturun
 */
public class GraphMoveCostCalculator implements PathFindingObject {

    @Override
    public float getCostForMove(Node fromNode, Node toNode) {
        return (float)fromNode.getWeight();
    }

    @Override
    public float getExtraCostForMove(Node fromNode, Node toNode) {
       
        
        float extraCost = 1.0f;
        /*if (fromNode.hasSwitch() && fromNode.getLaneId() != toNode.getLaneId()) {
           extraCost = 1.4f;
        }*/
        
        Edge edge = null;
        for (Edge e : fromNode.getToEdges()) {
            
            if (e.getTo().equals(toNode)) {
                edge = e;
                break;
            }
            
        }
        
       // extraCost *=  (float) fromNode.getPiece().getLengthForLane(fromNode.getLane());
        
        extraCost = 0f;
        if (edge != null)
            extraCost = (float) edge.getWeight();
        
        return extraCost;
    }
    
}
