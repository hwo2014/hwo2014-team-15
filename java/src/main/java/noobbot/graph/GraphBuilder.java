/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package noobbot.graph;

import java.util.LinkedList;
import java.util.List;

import noobbot.analyzer.RaceContainer;
import noobbot.graph.Edge.Direction;
import noobbot.objects.Race;
import noobbot.objects.Track;
import noobbot.objects.TrackLane;
import noobbot.objects.TrackPiece;

/**
 *
 * @author avilja
 */
public class GraphBuilder {

    public enum WeightingMethod {
        LENGTH,
        SPEED
    }
    
    private final Graph graph;

    
    private final Track track;
    private final TrackPiece[] pieces;
    private final TrackLane[] lanes;
    private final List<List<Node>> laneNodes;
    
    private GraphWeightCalculator weightCalculator;

    public GraphBuilder(RaceContainer raceContainer) {
        Race race = raceContainer.getRace();
    	
        this.graph = new Graph();
        this.track = race.track;
        this.lanes = race.track.lanes;
        this.pieces = race.track.pieces;
        laneNodes = new LinkedList<>();
        
        weightCalculator(new LengthBasedWeightCalculator(raceContainer));
    }

    public GraphBuilder weightCalculator(GraphWeightCalculator weightCalculator) {
        this.weightCalculator = weightCalculator;
        
        this.graph.setGraphWeightCalculator(weightCalculator);
        
        return this;
    }
    
    public Graph build() {

        for (int laneId = 0; laneId < lanes.length; laneId++) {
            laneNodes.add(new LinkedList<Node>());
        }

        for (int laneId = 0; laneId < lanes.length; laneId++) {
            List<Node> lane = laneNodes.get(laneId);
            for (int pieceId = 0; pieceId < pieces.length; pieceId++) {
                TrackPiece piece = pieces[pieceId];
                Node node = new Node().setLane(lanes[laneId]).setPiece(piece).setLaneId(laneId).setTrackPieceId(pieceId).setWeight(weightCalculator.getWeight(piece, pieceId, laneId));
                lane.add(node);
                //graph.addNode(node);
            }
        }

        for (List<Node> lane : laneNodes) {

            for (Node node : lane) {

                connectToNextNode(node, lane);
                connectToOtherLanes(node);

                graph.addNode(node);
            }

        }

        return graph;
    }

    private void connectToOtherLanes(Node node) {
        if (lanes.length > 1 && node.hasSwitch()) {
            if (node.getLaneId() > 0) {
                
                Node preceding = laneNodes.get(node.getLaneId() - 1).get(node.getTrackPieceId());
                if(preceding.getLane().distanceFromCenter < node.getLane().distanceFromCenter) { // Lefty
                    node.addEdge(new Edge(node, preceding, weightCalculator.getWeightForEdge(node, Direction.LEFT_TURN)), Direction.LEFT_TURN);
                    System.out.println("Lefty to " + node.getTrackPieceId());
                } else {
                    node.addEdge(new Edge(node, preceding, weightCalculator.getWeightForEdge(node, Direction.RIGHT_TURN)), Direction.RIGHT_TURN);
                    System.out.println("Righty to " + node.getTrackPieceId());
                }
                //node.addEdge(new Edge(node, laneNodes.get(node.getLaneId() - 1).get(node.getTrackPieceId()), 0), Direction.LEFT_TURN);
            }

            if (node.getLaneId() < lanes.length - 1) {
                
                Node following = laneNodes.get(node.getLaneId() + 1).get(node.getTrackPieceId());
                if(following.getLane().distanceFromCenter > node.getLane().distanceFromCenter) { // Righty
                    node.addEdge(new Edge(node, following, weightCalculator.getWeightForEdge(node, Direction.RIGHT_TURN)), Direction.RIGHT_TURN);//101.98
                    System.out.println("Righty to " + node.getTrackPieceId());
                } else {
                    node.addEdge(new Edge(node, following, weightCalculator.getWeightForEdge(node, Direction.LEFT_TURN)), Direction.LEFT_TURN);
                    System.out.println("Lefty to " + node.getTrackPieceId());
                }
                //node.addEdge(new Edge(node, laneNodes.get(node.getLaneId() + 1).get(node.getTrackPieceId()), 0), Direction.RIGHT_TURN);
            }

        }
    }

    private void connectToNextNode(Node node, List<Node> lane) {
        if (node.getTrackPieceId() >= pieces.length - 1) {
            node.addEdge(new Edge(node, lane.get(0), weightCalculator.getWeightForEdge(node, Direction.STRAIGHT)), Direction.STRAIGHT);
            //  System.out.println("connectToNextNode | node.getTrackPieceId() >= lanes.length - 2 : " + node);
        } else {
            node.addEdge(new Edge(node, lane.get(node.getTrackPieceId() + 1), weightCalculator.getWeightForEdge(node, Direction.STRAIGHT)), Direction.STRAIGHT);
            //  System.out.println("connectToNextNode | else : " + node);
        }
    }

}
