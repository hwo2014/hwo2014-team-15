/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package noobbot.graph;

import com.louhigames.graph.algorithms.pathfinding.PathFindingCostComparator;
import com.louhigames.graph.algorithms.pathfinding.astar.ShortestLengthAStarCostComparator;

import noobbot.analyzer.RaceContainer;
import noobbot.objects.TrackLane;
import noobbot.objects.TrackPiece;

/**
 *
 * @author teturun
 */
public class LengthBasedWeightCalculator implements GraphWeightCalculator {

	public final PathFindingCostComparator costComparator = new ShortestLengthAStarCostComparator();
	
    private final TrackLane[] lanes;
    
    public LengthBasedWeightCalculator(RaceContainer raceContainer) {
        this.lanes = raceContainer.getRace().track.lanes;
    }

    
    @Override
    public double getWeight(TrackPiece piece, int pieceIndex, int laneId) {
        
        return piece.getLengthForLane(this.lanes[laneId]);
        
    }

    @Override
    public double getWeightForEdge(Node n, Edge.Direction direction) {
        
        double weight = 0.0;
        switch (direction) {
            case LEFT_TURN:
                weight = 2;
                break;
            case RIGHT_TURN:
                weight = 2;
                break;
            case STRAIGHT:
                weight = 1;
                break;
        }
        
        return weight;
    }


	@Override
	public PathFindingCostComparator getCostComparator() {
		return costComparator;
	}

    
}
