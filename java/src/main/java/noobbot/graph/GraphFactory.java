/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package noobbot.graph;

import com.google.gson.Gson;
import noobbot.analyzer.RaceContainer;
import noobbot.objects.Race;

/**
 *
 * @author teturun
 */
public class GraphFactory {

    private static final String KEIMOLA_TRACK_JSON = "{\"track\":{\"id\":\"keimola\",\"name\":\"Keimola\",\"pieces\":[{\"length\":100.0},{\"length\":100.0},{\"length\":100.0},{\"length\":100.0,\"switch\":true},{\"radius\":100.0,\"angle\":45.0},{\"radius\":100.0,\"angle\":45.0},{\"radius\":100.0,\"angle\":45.0},{\"radius\":100.0,\"angle\":45.0},{\"radius\":200.0,\"angle\":22.5,\"switch\":true},{\"length\":100.0},{\"length\":100.0},{\"radius\":200.0,\"angle\":-22.5},{\"length\":100.0},{\"length\":100.0,\"switch\":true},{\"radius\":100.0,\"angle\":-45.0},{\"radius\":100.0,\"angle\":-45.0},{\"radius\":100.0,\"angle\":-45.0},{\"radius\":100.0,\"angle\":-45.0},{\"length\":100.0,\"switch\":true},{\"radius\":100.0,\"angle\":45.0},{\"radius\":100.0,\"angle\":45.0},{\"radius\":100.0,\"angle\":45.0},{\"radius\":100.0,\"angle\":45.0},{\"radius\":200.0,\"angle\":22.5},{\"radius\":200.0,\"angle\":-22.5},{\"length\":100.0,\"switch\":true},{\"radius\":100.0,\"angle\":45.0},{\"radius\":100.0,\"angle\":45.0},{\"length\":62.0},{\"radius\":100.0,\"angle\":-45.0,\"switch\":true},{\"radius\":100.0,\"angle\":-45.0},{\"radius\":100.0,\"angle\":45.0},{\"radius\":100.0,\"angle\":45.0},{\"radius\":100.0,\"angle\":45.0},{\"radius\":100.0,\"angle\":45.0},{\"length\":100.0,\"switch\":true},{\"length\":100.0},{\"length\":100.0},{\"length\":100.0},{\"length\":90.0}],\"lanes\":[{\"distanceFromCenter\":-10.0,\"index\":0.0},{\"distanceFromCenter\":10.0,\"index\":1.0}],\"startingPoint\":{\"position\":{\"x\":-300.0,\"y\":-44.0},\"angle\":90.0}},\"cars\":[{\"id\":{\"name\":\"teemubot-0\",\"color\":\"red\"},\"dimensions\":{\"length\":40.0,\"width\":20.0,\"guideFlagPosition\":10.0}}],\"raceSession\":{\"laps\":3.0,\"maxLapTimeMs\":60000.0,\"quickRace\":true}}";
    private static final String KEIMOLA_LYHENNETTY = "{\"track\":{\"id\":\"keimola\",\"name\":\"Keimola\",\"pieces\":[{\"length\":100.0},{\"length\":100.0},{\"length\":100.0},{\"length\":100.0,\"switch\":true},{\"radius\":100.0,\"angle\":45.0},{\"length\":90.0}],\"lanes\":[{\"distanceFromCenter\":-10.0,\"index\":0.0},{\"distanceFromCenter\":10.0,\"index\":1.0}],\"startingPoint\":{\"position\":{\"x\":-300.0,\"y\":-44.0},\"angle\":90.0}},\"cars\":[{\"id\":{\"name\":\"teemubot-0\",\"color\":\"red\"},\"dimensions\":{\"length\":40.0,\"width\":20.0,\"guideFlagPosition\":10.0}}],\"raceSession\":{\"laps\":3.0,\"maxLapTimeMs\":60000.0,\"quickRace\":true}}";
    private static final String USA_TRACK_JSON = "{\"track\":{\"id\":\"usa\",\"name\":\"USA\",\"pieces\":[{\"length\":100.0},{\"length\":100.0,\"switch\":true},{\"radius\":200.0,\"angle\":22.5},{\"radius\":200.0,\"angle\":22.5},{\"radius\":200.0,\"angle\":22.5},{\"radius\":200.0,\"angle\":22.5},{\"length\":100.0},{\"length\":100.0,\"switch\":true},{\"radius\":200.0,\"angle\":22.5},{\"radius\":200.0,\"angle\":22.5},{\"radius\":200.0,\"angle\":22.5},{\"radius\":200.0,\"angle\":22.5},{\"length\":100.0},{\"length\":100.0},{\"length\":100.0},{\"length\":100.0},{\"length\":100.0},{\"length\":100.0,\"switch\":true},{\"radius\":200.0,\"angle\":22.5},{\"radius\":200.0,\"angle\":22.5},{\"radius\":200.0,\"angle\":22.5},{\"radius\":200.0,\"angle\":22.5},{\"length\":100.0},{\"length\":100.0,\"switch\":true},{\"radius\":200.0,\"angle\":22.5},{\"radius\":200.0,\"angle\":22.5},{\"radius\":200.0,\"angle\":22.5},{\"radius\":200.0,\"angle\":22.5},{\"length\":100.0},{\"length\":100.0},{\"length\":100.0},{\"length\":100.0}],\"lanes\":[{\"distanceFromCenter\":-20.0,\"index\":0.0},{\"distanceFromCenter\":0.0,\"index\":1.0},{\"distanceFromCenter\":20.0,\"index\":2.0}],\"startingPoint\":{\"position\":{\"x\":-340.0,\"y\":-96.0},\"angle\":90.0}},\"cars\":[{\"id\":{\"name\":\"louhibot-0\",\"color\":\"red\"},\"dimensions\":{\"length\":40.0,\"width\":20.0,\"guideFlagPosition\":10.0}}],\"raceSession\":{\"laps\":3.0,\"maxLapTimeMs\":60000.0,\"quickRace\":true}}";
    
    public enum TestTrack {

        KEIMOLA(KEIMOLA_TRACK_JSON),
        USA(USA_TRACK_JSON),
        KEIMOLA_SHORT(KEIMOLA_LYHENNETTY);

        private String json;

        private TestTrack(String json) {
            this.json = json;
        }

        public String getJson() {
            return json;
        }
    }
    
    public static Race getTestRace(TestTrack track) {
        final Gson gson = new Gson();

        return gson.fromJson(track.getJson(), Race.class);
    }

    public static Graph getGraphFrom(RaceContainer raceContainer) {
        return new GraphBuilder(raceContainer).build();
    }
    
//    public static Graph getTestGraph(TestTrack track) {
//        final Gson gson = new Gson();
//
//        final Race race = gson.fromJson(track.getJson(), Race.class);
//
//        return new GraphBuilder(race).build();
//    }
//
//    public static void main(String... args) {
//
//        final Gson gson = new Gson();
//
//        String trackJson = KEIMOLA_TRACK_JSON;
//        final Race race = gson.fromJson(trackJson, Race.class);
//
//        
//        System.out.println(new GraphBuilder(race).build().toString());
//
//    }

}
