/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package noobbot.graph;

/**
 *
 * @author avilja
 */
public class Edge {
 
    public enum Direction {
        STRAIGHT,
        LEFT_TURN,
        RIGHT_TURN
    }
    
    private final Node from;
    private final Node to;
    private double weight;
    
    public Edge(Node from, Node to, double weight) {
        this.from = from;
        this.to = to;
        this.weight = weight;
    }

    public Node getFrom() {
        return from;
    }

    public Node getTo() {
        return to;
    }

    public double getWeight() {
        return weight;
    }
    
    public void setWeight(double weight) {
        this.weight = weight;
    }

    @Override
    public String toString() {
        return "Edge " + this.hashCode() + " : from: " + from.getTrackPieceId() + " " + from.getLaneId() + " to " + to.getTrackPieceId() + " " + to.getLaneId();
    }
    
    
    
}
