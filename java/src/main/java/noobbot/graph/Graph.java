/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package noobbot.graph;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;
import noobbot.graph.Edge.Direction;

/**
 *
 * @author avilja
 */
public class Graph {

    private final List<Node> nodes;
    private final List<Edge> edges;
    
    private GraphWeightCalculator weightCalculator;
    
    public Graph() {
        this.nodes = new LinkedList<>();
        this.edges = new LinkedList<>();
    }

    public void setGraphWeightCalculator(GraphWeightCalculator weightCalculator) {
        this.weightCalculator = weightCalculator;
    }
    
    public List<Node> getNodes() {
        return nodes;
    }

    public List<Edge> getEdges() {
        return edges;
    }
    
    public Node getNode(double trackPieceId, double laneId) {
        
        for (Node n : getNodes()) {
            
            if (n.getTrackPieceId() == trackPieceId && n.getLaneId() == laneId) {
                return n;
            }
            
        }
        
        return null;
    }

    public Node getPreviousNode(Node node) {
        
        if (node == null) return null;
        
        double nodeTrackPieceId = node.getTrackPieceId();
        double nodeLaneId = node.getLaneId();
        
        for (Edge e : edges) {
            
            double trackPieceId = e.getTo().getTrackPieceId();
            double laneId = e.getTo().getLaneId();
            
            if (trackPieceId == nodeTrackPieceId && nodeLaneId == laneId) {
                return e.getFrom();
            }
            
        }
        
        return null;
    }
    
    /**
     * Returns immediate previous nodes
     * 
     * @param node
     * @return 
     */
    public List<Node> getPreviousNodes(Node node) {
        
        List<Node> nodes = new ArrayList<>();
        double nodeTrackPieceId = node.getTrackPieceId();
        
         for (Edge e : edges) {
            
            double trackPieceId = e.getTo().getTrackPieceId();
            
            if (trackPieceId == nodeTrackPieceId) {
                nodes.add(e.getFrom());
            }
            
        }
        
         return nodes;
    }
    
    public void addNode(Node node) {
        this.nodes.add(node);
        this.edges.addAll(node.getToEdges());
    }

    public void refreshWeights() {
        
        for (Node n : nodes) {
            
            n.setWeight(weightCalculator.getWeight(n.getPiece(), n.getTrackPieceId(), n.getLaneId()));
            
            for (Entry<Direction, Edge> entry : n.getEdges().entrySet()) {
                
                Direction d = entry.getKey();
                Edge e = entry.getValue();
                
                e.setWeight(weightCalculator.getWeightForEdge(n, d));
                
            }
            
        }
       
    }
    
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("Graph: \n");
        for(Node node : nodes) {
            builder.append(node.toString() + "\n");          
        }
        builder.append("\n\n\n");
         for(Edge edge : edges) {
              builder.append(edge.toString() + "\n");
         }

        
        return builder.toString();
    }

	public GraphWeightCalculator getWeightCalculator() {
		return weightCalculator;
	}
    
    

}
