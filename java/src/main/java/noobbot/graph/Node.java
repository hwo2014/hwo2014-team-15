/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package noobbot.graph;

import com.louhigames.graph.algorithms.pathfinding.astar.AStarNode;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import noobbot.graph.Edge.Direction;
import noobbot.objects.TrackLane;
import noobbot.objects.TrackPiece;

/**
 *
 * @author avilja
 */
public class Node {
    
    private TrackPiece piece;
    private TrackLane lane;

    private int laneId;
    private int trackPieceId;
    
    private final HashMap<Direction, Edge> edges;

    private final List<Edge> toEdges;
    private final List<Node> neighbors;
    
    public double weight = 0.0;
    
    public Node() {
        this.toEdges = new LinkedList<>();
        this.neighbors = new LinkedList<>();
        this.edges = new HashMap<>();
    }

    public double getWeight() {
        return weight;
    }

    public Node setWeight(double weight) {
        this.weight = weight;
        
        return this;
    }
    
    public TrackPiece getPiece() {
        return piece;
    }

    public Node setPiece(TrackPiece piece) {
        this.piece = piece;
        return this;
    }
    
    public Node setLane(TrackLane lane) {
        this.lane = lane;
        return this;
    }
    
    public TrackLane getLane() {
        return lane;
    }

    public List<Edge> getToEdges() {
        return toEdges;
    }
    
    public HashMap<Direction, Edge> getEdges() {
        return edges;
    }
    
    public Node addEdge(Edge edge, Direction direction) {
        this.edges.put(direction, edge);
        this.toEdges.add(edge);
        this.neighbors.add(edge.getTo());
        return this;
    }

    public int getLaneId() {
        return laneId;
    }

    public Node setLaneId(int laneId) {
        this.laneId = laneId;
        return this;
    }

    public int getTrackPieceId() {
        return trackPieceId;
    }

    public Node setTrackPieceId(int trackPieceId) {
        this.trackPieceId = trackPieceId;
        return this;
    }

    public List<Node> getNeighbors() {
        return this.neighbors;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 53 * hash + this.laneId;
        hash = 53 * hash + this.trackPieceId;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass() && obj instanceof AStarNode == false) {
            return false;
        }
       
        final Node other = obj instanceof AStarNode ? ((AStarNode)obj).getOriginalNode() : (Node) obj;
        if (this.laneId != other.laneId) {
            return false;
        }
        if (this.trackPieceId != other.trackPieceId) {
            return false;
        }
        return true;
    }
    
    
    
    boolean detailedPrint = false;
    
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("Node trackPieceId=" + (this.trackPieceId +1) + " laneId=" + this.laneId + " weight=" + this.getWeight() + " " + piece.hasSwitch + (detailedPrint ? " " + piece + " " + lane + " " : ""));
        
       /* for (Edge edge : toEdges) {
            builder.append("\t\nEdge " + edge);
        }*/
        
        return builder.toString();
    }

    public boolean hasSwitch() {
        return this.piece.hasSwitch;
    }
    
    
           
}
