/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package noobbot.graph;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import noobbot.analyzer.Calculator;
import noobbot.analyzer.RaceContainer;
import noobbot.analyzer.waypoints.Waypoint;
import noobbot.objects.TrackPiece;

import com.louhigames.graph.algorithms.pathfinding.PathFindingCostComparator;
import com.louhigames.graph.algorithms.pathfinding.astar.FastestPathAStarCostComparator;

/**
 *
 * @author teturun
 */
public class SpeedBasedWeightCalculator implements GraphWeightCalculator {

	public final PathFindingCostComparator costComparator = new FastestPathAStarCostComparator();

    private RaceContainer raceContainer;
    private Calculator calculator;
    
    public SpeedBasedWeightCalculator(RaceContainer raceContainer) {
        this.raceContainer = raceContainer;
        this.calculator = new Calculator(raceContainer);
    }
    
    @Override
    public double getWeight(TrackPiece piece, int pieceIndex, int laneId) {
        
        double weight = 0.0;
        
//        List<Waypoint> waypoints = getWaypoints(pieceIndex);
//        
//        if (waypoints == null || waypoints.isEmpty()) return weight;
//        
//        
//        TrackLane lane = raceContainer.getRace().track.lanes[laneId];
//        
//        double speedForAvgLane = calculator.approximateTargetSpeed(piece);
//        double speedForLane = calculator.approximateTargetSpeed(piece, laneId);
//        double f = speedForLane / speedForAvgLane;
        
        double cornerTotalAngle = raceContainer.getRace().track.cornerTotalAngle(pieceIndex);
        double cornerTotalLength = raceContainer.getRace().track.cornerTotalLength(pieceIndex, laneId);
        double cornersAverageAngle = raceContainer.getRace().track.cornersAverageAngle;
        double cornersAverageLength = raceContainer.getRace().track.cornersAverageLength;
        
        int averageLaneIndex = raceContainer.getRace().track.getAverageLaneIndex();
        
        boolean tighter = false;
        boolean shorter = false;
        
        if (cornerTotalAngle > cornersAverageAngle)
        	tighter = true;
        	
        if (cornerTotalLength < cornersAverageLength)
        	shorter = false;
        
        if (tighter && shorter) {
        	// prefer outer lane (smoother turn)
        	if (laneId > averageLaneIndex) {
            	weight = 1;
        	} else {
            	weight = 2;        		        		
        	}
        }
        else if (!tighter && !shorter) {
        	// prefer inner lane (shorter path)
        	if (laneId > averageLaneIndex) {
            	weight = 2;        		
        	} else {
            	weight = 1;
        	}
        } else {
        	weight = 0;
        }
        
        
//        /*double observedSpeed = 0.0;
//        for (Waypoint wp : waypoints) {
//            
//           // waypoints.re
//            if (wp.observedSpeed > 0.0) {
//                observedSpeed += wp.observedSpeed;
//            }
//            else {
//                observedSpeed += wp.approximatedTargetSpeed;
//            }
//            
//            
//        }
//       
//        observedSpeed = observedSpeed / ((double)waypoints.size());
//       */
//        
//        
//       // System.out.println("WEIGHT FOR " + pieceIndex + " " + laneId + " = " + speedSum);
//        
//        weight = speedForLane; //observedSpeed * f;// / ((double)waypoints.size()); // avg of speeds
        
        return weight;
    }

    private List<Waypoint> getWaypoints(double pieceIndex) {
        
        List<Waypoint> waypoints = new ArrayList<>();
        
        TreeMap<Double, Waypoint> waypointMap = raceContainer.getWaypoints().waypointMap;
        
        for (Map.Entry<Double,Waypoint> entry : waypointMap.entrySet()) {
            
            Waypoint wp = entry.getValue();
            
            if (wp.pieceIndex == pieceIndex) {
                waypoints.add(wp);
            }
            
        }
        
        
        return waypoints;
    }
    
    @Override
    public double getWeightForEdge(Node n, Edge.Direction direction) {
                
        double weight = 0.0; // TODO figure some value
        
        switch (direction) {
            case STRAIGHT:
                weight = 2;
                break;
            default:
                weight = 1;
        }
       
        return weight;
        
    }

	@Override
	public PathFindingCostComparator getCostComparator() {
		return costComparator;
	}

}
