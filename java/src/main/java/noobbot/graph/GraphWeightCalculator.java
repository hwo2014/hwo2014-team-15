/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package noobbot.graph;

import noobbot.graph.Edge.Direction;
import noobbot.objects.TrackPiece;

import com.louhigames.graph.algorithms.pathfinding.PathFindingCostComparator;

/**
 *
 * @author teturun
 */
public interface GraphWeightCalculator {
    
    public double getWeight(TrackPiece piece, int pieceIndex, int laneId);
    public double getWeightForEdge(Node n, Direction direction);
    
    public PathFindingCostComparator getCostComparator();
}
