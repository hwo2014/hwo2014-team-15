/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package noobbot.events;

/**
 *
 * @author teturun
 */
public interface TrackListener {
    
    public void carEnteredTrackPiece(CarEnteredTrackPieceEvent e);
    public void carSwitchedLanes(CarSwitchedLaneEvent e);
    public void carFinishedLap(CarFinishedLapEvent e);
    
}
