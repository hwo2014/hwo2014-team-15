/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package noobbot.events;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import noobbot.analyzer.RaceContainer;
import noobbot.objects.Car;
import noobbot.objects.CarPosition;

/**
 *
 * @author teturun
 */
public class TrackObserver {
 
    private List<TrackListener> listeners;
    private RaceContainer race;
    
    public TrackObserver(RaceContainer race) {
        this.listeners = new ArrayList<>();
        this.race = race;
    }
    
    public void addTrackListener(TrackListener listener) {
        this.listeners.add(listener);
    }
    
    public void tick() {
        
        for (Car c : race.getRace().cars) {
            
            hasEnteredNewPiece(c);
            hasSwitchedLanes(c);
            
        }
        
    }
    
    private void hasEnteredNewPiece(Car car) {
        
        LinkedList<CarPosition> positions = race.getAllPositions().get(car.id.name);
        
        if (positions == null) return;
        
        CarPosition latest = positions.getLast();
        CarPosition previous = latest;
        
        if (positions.size()-2 > 0) {
            previous = positions.get(positions.size()-2);
        }
        
        if (latest.currentIndex() != previous.currentIndex()) {

            CarEnteredTrackPieceEvent e = new CarEnteredTrackPieceEvent(car, race.currentPiece(previous), race.currentPiece(latest));
            notifyListeners(e);
 
        }
         
        if (latest.piecePosition.lap != previous.piecePosition.lap) {
            
            double lapTimeMs = 0.0;
            CarFinishedLapEvent f = new CarFinishedLapEvent(car, latest.piecePosition.lap, lapTimeMs);

            notifyListeners(f);
        }
        
    }
    
    private void notifyListeners(CarEnteredTrackPieceEvent e) {
        
        for (TrackListener tl : listeners) {
            tl.carEnteredTrackPiece(e);
        }
        
    }
    
    private void notifyListeners(CarFinishedLapEvent e) {
        
        for (TrackListener tl : listeners) {
            tl.carFinishedLap(e);
        }
        
    }
    
    private void hasSwitchedLanes(Car car) {
        
        LinkedList<CarPosition> positions = race.getAllPositions().get(car.id.name);
        
        if (positions == null) return;
        
        CarPosition latest = positions.getLast();
        CarPosition previous = latest;
        
        if (positions.size()-2 > 0) {
            previous = positions.get(positions.size()-2);
        }
        
        if (! race.getCurrentTrackLane(latest).equals(race.getCurrentTrackLane(previous))) {
            
            CarSwitchedLaneEvent e = new CarSwitchedLaneEvent(car, race.currentPiece(latest), race.getCurrentTrackLane(previous), race.getCurrentTrackLane(latest));
            notifyListeners(e);
            
        }
        
        
    }
    
    
    private void notifyListeners(CarSwitchedLaneEvent e) {
        
        for (TrackListener tl : listeners) {
            tl.carSwitchedLanes(e);
        }
        
    }
    
    
}
