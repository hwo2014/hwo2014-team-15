/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package noobbot.events;

import noobbot.MsgWrapper;
import noobbot.objects.CarPositions;
import noobbot.objects.Id;
import noobbot.objects.Race;
import noobbot.objects.Turbo;

/**
 *
 * @author teturun
 */
public interface MessageListener {
    
    public void carPositionsReceived(CarPositions carPositions);
    public void joinReceived(Object data);
    public void gameInitReceived(Race race);
    public void gameStartReceived(Object data);
    public void gameEndReceived(Object data);
    public void dnfReceived(Object data);
    public void yourCarReceived(Object data);
    public void lapFinishedReceived(Object data);
    public void finishReceived(Id carId);
    public void tournamentEndReceived(Object data);
    public void crashReceived(Id carId);
    public void spawnReceived(Id carId);
    public void turboAvailableReceived(Turbo turbo);
    public void unidentifiedMessageReceived(MsgWrapper message);
    
}
