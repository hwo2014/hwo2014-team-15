/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package noobbot.events;

import com.google.gson.Gson;
import com.google.gson.internal.LinkedTreeMap;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import noobbot.MsgWrapper;
import noobbot.TickMsgWrapper;
import noobbot.objects.CarPosition;
import noobbot.objects.CarPositions;
import noobbot.objects.Id;
import noobbot.objects.Race;
import noobbot.objects.Turbo;

/**
 *
 * @author teturun
 */
public class MessageHandler {
    
    enum MessageType {
        CAR_POSITIONS,
        JOIN,
        GAME_INIT,
        GAME_START,
        GAME_END,
        DNF,
        YOUR_CAR,
        LAP_FINISHED,
        FINISH,
        TOURNAMENT_END,
        CRASH,
        SPAWN,
        TURBO_AVAILABLE,
        UNIDENTIFIED
    }
    
    final Gson gson = new Gson();
    
    private final List<MessageListener> listeners;
    private boolean listen = false;
   
    
    public MessageHandler() {
        listeners = new ArrayList<>();
    }
    
    public void addListener(MessageListener listener) {
        listeners.add(listener);
    }
    
    public void stopListening() {
        this.listen = false;
    }
    
    public void startListening(final BufferedReader reader) throws IOException {
        
        listen = true;
        String line = null;
        //boolean raceHasStarted = false;
        
        while((line = reader.readLine()) != null && listen) {
            
            try {
                
                final TickMsgWrapper msgFromServer = gson.fromJson(line, TickMsgWrapper.class);

                MessageType type = MessageType.UNIDENTIFIED;
                Object data = null;

                //System.out.println("Race has started=" + raceHasStarted);
                if (msgFromServer.msgType.equals("carPositions")) {

                   // System.out.println(msgFromServer);
                    int gameTick = msgFromServer.gameTick;

                    ArrayList dataList = (ArrayList) msgFromServer.data;

                    ArrayList<CarPosition> carPositionList = new ArrayList<>();

                    for (Object dataObject : dataList) {
                        LinkedTreeMap ltm = (LinkedTreeMap) dataObject;
                        String json = gson.toJson(ltm);
                        final CarPosition carPos = gson.fromJson(json, CarPosition.class);

                        carPositionList.add(carPos);
                    }

                    CarPositions carPositions = new CarPositions(carPositionList, gameTick);

                    type = MessageType.CAR_POSITIONS;
                    data = carPositions;

                } else if (msgFromServer.msgType.equals("join")) {

                    type = MessageType.JOIN;
                    data = msgFromServer.data;

                    //System.out.println("[" + botName + "]: Joined");
                } else if (msgFromServer.msgType.equals("gameInit")) {
                    //System.out.println("[" + botName + "]: Race init");

                    LinkedTreeMap dataTree = (LinkedTreeMap) msgFromServer.data;
                    Object raceObj = dataTree.get("race");
                    String json = gson.toJson(raceObj);

                    //System.out.println(json);

                    final Race race = gson.fromJson(json, Race.class);

                    type = MessageType.GAME_INIT;
                    data = race;

                } else if (msgFromServer.msgType.equals("gameEnd")) {
                    //System.out.println("[" + botName + "]: Race end" + ", data=" + msgFromServer.data);

                    type = MessageType.GAME_END;
                    data = msgFromServer.data;

                } else if (msgFromServer.msgType.equals("gameStart")) {
                    //System.out.println("[" + botName + "]: Race start" + ", data=" + msgFromServer.data);
                   // raceHasStarted = true;

                    type = MessageType.GAME_START;
                    data = msgFromServer.data;

                    //send(new Ping());
                } else if (msgFromServer.msgType.equals("dnf")) {
                    //System.out.println("[" + botName + "]: We were disqualified" + ", data=" + msgFromServer.data);

                    type = MessageType.DNF;
                    data = msgFromServer.data;

                } else if (msgFromServer.msgType.equals("yourCar")) {
                    //System.out.println("[" + botName + "]: My car info" + ", data=" + msgFromServer.data);

                    type = MessageType.YOUR_CAR;
                    data = msgFromServer.data;

                } else if (msgFromServer.msgType.equals("lapFinished")) {
                    //System.out.println("[" + botName + "]: Lap finished" + ", data=" + msgFromServer.data);

                    type = MessageType.LAP_FINISHED;
                    data = msgFromServer.data;

                } else if (msgFromServer.msgType.equals("finish")) {
                   // System.out.println("[" + botName + "]: Finish" + ", data=" + msgFromServer.data);

                    type = MessageType.FINISH;

                    LinkedTreeMap dataTree = (LinkedTreeMap) msgFromServer.data;
                    String json = gson.toJson(dataTree);
                    //System.out.println(json);

                    final Id id = gson.fromJson(json, Id.class);

                    data = id;

                } else if (msgFromServer.msgType.equals("tournamentEnd")) {
                    //System.out.println("[" + botName + "]: Tournament end" + ", data=" + msgFromServer.data);
                    type = MessageType.TOURNAMENT_END;
                    data = msgFromServer.data;

                } else if (msgFromServer.msgType.equals("crash")) {
                   // System.out.println("[" + botName + "]: ################# CRASH CRASH CRASH ################# " + ", data=" + msgFromServer.data);

                    type = MessageType.CRASH;

                    LinkedTreeMap dataTree = (LinkedTreeMap) msgFromServer.data;
                    String json = gson.toJson(dataTree);
                    //System.out.println(json);

                    final Id id = gson.fromJson(json, Id.class);

                    data = id;

                } else if (msgFromServer.msgType.equals("spawn")) {
                   // System.out.println("[" + botName + "]: Spawn" + ", data=" + msgFromServer.data);
                    type = MessageType.SPAWN;
                    LinkedTreeMap dataTree = (LinkedTreeMap) msgFromServer.data;
                    String json = gson.toJson(dataTree);

                    final Id id = gson.fromJson(json, Id.class);

                    data = id;

                } else if (msgFromServer.msgType.equals("turboAvailable")) {

                    LinkedTreeMap dataTree = (LinkedTreeMap) msgFromServer.data;
                    String json = gson.toJson(dataTree);
                    //System.out.println(json);

                    final Turbo turbo = gson.fromJson(json, Turbo.class);

                    type = MessageType.TURBO_AVAILABLE;
                    data = turbo;

                }
                else {
                    //System.out.println("[" + botName + "]: UNHANDLED msgFromServer.msgType " + msgFromServer.msgType + ", data=" + msgFromServer.data);

                    type = MessageType.UNIDENTIFIED;
                    data = msgFromServer;

                   // send(new Ping());
                }

                notify(type, data);

            } catch (Exception e) {
                System.out.println("Something is bugging me. Stop it! Okay here's the message: " + e.getMessage());
            }
            
            
            
        }
        
    }
    
    private void notify(MessageType type, Object data) {
        
        for (MessageListener l : listeners) {
            
            switch (type) {
            
                case CAR_POSITIONS:
                    l.carPositionsReceived((CarPositions) data);
                    break;
                case JOIN:
                    l.joinReceived(data);
                    break;
                case GAME_INIT:
                    l.gameInitReceived((Race) data);
                    break;
                case GAME_START:
                    l.gameStartReceived(data);
                    break;
                case GAME_END:
                    l.gameEndReceived(data);
                    break;
                case DNF:
                    l.dnfReceived(data);
                    break;
                case YOUR_CAR:
                    l.yourCarReceived(data);
                    break;
                case LAP_FINISHED:
                    l.lapFinishedReceived(data);
                    break;
                case FINISH:
                    l.finishReceived((Id) data);
                    break;
                case TOURNAMENT_END:
                    l.tournamentEndReceived(data);
                    break;
                case CRASH:
                    l.crashReceived((Id) data);
                    break;
                case SPAWN:
                    l.spawnReceived((Id) data);
                    break;
                case TURBO_AVAILABLE:
                    l.turboAvailableReceived((Turbo) data);
                    break;
                case UNIDENTIFIED:
                    l.unidentifiedMessageReceived((MsgWrapper) data);
                    break;
            
            }
            
        }
        
    }    
    
}
