package noobbot.events;

import noobbot.objects.Car;
import noobbot.objects.TrackPiece;

/**
 *
 * @author teturun
 */
public class CarEnteredTrackPieceEvent {
    
    private final Car car;
    private final TrackPiece from;
    private final TrackPiece to;
    
    public CarEnteredTrackPieceEvent(final Car car, final TrackPiece from, final TrackPiece to) {
        this.car = car;
        this.from = from;
        this.to = to;
    }

    public Car getCar() {
        return car;
    }

    public TrackPiece getFrom() {
        return from;
    }

    public TrackPiece getTo() {
        return to;
    }

    @Override
    public String toString() {
        return "CarEnteredTrackPieceEvent{" + "car=" + car + ", from=" + from + ", to=" + to + '}';
    }
    
    
    
}
