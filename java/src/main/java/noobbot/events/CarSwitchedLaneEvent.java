/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package noobbot.events;

import noobbot.objects.Car;
import noobbot.objects.TrackLane;
import noobbot.objects.TrackPiece;

/**
 *
 * @author teturun
 */
public class CarSwitchedLaneEvent {
    
    private final Car car;
    private final TrackPiece piece;
    private final TrackLane from;
    private final TrackLane to;
    
    public CarSwitchedLaneEvent(Car car, TrackPiece piece, TrackLane from, TrackLane to) {
        this.car = car;
        this.piece = piece;
        this.from = from;
        this.to = to;
    }

    public Car getCar() {
        return car;
    }

    public TrackPiece getPiece() {
        return piece;
    }

    public TrackLane getFrom() {
        return from;
    }

    public TrackLane getTo() {
        return to;
    }

    @Override
    public String toString() {
        return "CarSwitchedLaneEvent{" + "car=" + car + ", piece=" + piece + ", from=" + from + ", to=" + to + '}';
    }
    
    
    
}
