/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package noobbot.events;

import noobbot.objects.Car;

/**
 *
 * @author teturun
 */
public class CarFinishedLapEvent {
    
    private final Car car;
    private double newLapNr;
    private double lapTimeMs;
    
    public CarFinishedLapEvent(final Car car, final double newLapNro, final double lapTimeMs) {
        this.car = car;
        this.newLapNr = newLapNr;
        this.lapTimeMs = lapTimeMs;
    }

    public Car getCar() {
        return car;
    }

    public double getNewLapNr() {
        return newLapNr;
    }

    public double getLapTimeMs() {
        return lapTimeMs;
    }
    
    
}
