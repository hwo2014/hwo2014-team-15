package noobbot.analyzer;

import java.util.HashMap;
import java.util.LinkedList;

import noobbot.analyzer.state.CarState;
import noobbot.analyzer.state.SpeedChangeStates;
import noobbot.analyzer.waypoints.Waypoint;
import noobbot.analyzer.waypoints.Waypoints;
import noobbot.graph.Graph;
import noobbot.graph.GraphBuilder;
import noobbot.graph.LengthBasedWeightCalculator;
import noobbot.objects.CarPosition;
import noobbot.objects.Lane;
import noobbot.objects.Race;
import noobbot.objects.TrackLane;
import noobbot.objects.TrackPiece;
import noobbot.objects.Turbo;

import com.louhigames.graph.algorithms.pathfinding.SearchResult;

public class RaceContainer {

    private Race race;
    private Graph raceGraph;
    private SearchResult currentPath;
    
    private Waypoints waypoints;
    private SpeedChangeStates speedChangeStates;

    private HashMap<String, LinkedList<CarPosition>> allPositions;
    private HashMap<String, LinkedList<CarState>> allStates;
    
    private Turbo turbo;
    
    private boolean raceStarted = false;
    private boolean currentlyCrashed = false;

    private final String botName;
    
    
    public RaceContainer(String botName) {
    	this.botName = botName;
    	
        this.speedChangeStates = new SpeedChangeStates();
    }

    public SearchResult getPath() {
        return currentPath;
    }

    public void setPath(SearchResult path) {
        this.currentPath = path;
    }

    public void setrace(Race race) {
        this.race = race;

        this.waypoints = new Waypoints(this).initializeWaypoints();
        // this.raceGraph = new GraphBuilder(this).weightCalculator(new SpeedBasedWeightCalculator(this)).build(); // This potentially takes *some* time, not much.
        this.raceGraph = new GraphBuilder(this).weightCalculator(new LengthBasedWeightCalculator(this)).build(); // This potentially takes *some* time, not much.
    }
    
    public Race getRace() {
		return race;
	}

	public Graph getRaceGraph() {
        return raceGraph;
    }
    
    public Waypoints getWaypoints() {
		return waypoints;
	}

    public SpeedChangeStates getSpeedChangeStates() {
		return speedChangeStates;
	}
	
    public HashMap<String, LinkedList<CarPosition>> getAllPositions() {
    	if (this.allPositions == null)
    		this.allPositions = new HashMap<String, LinkedList<CarPosition>>();
        
		return allPositions;
	}
	
    public HashMap<String, LinkedList<CarState>> getAllStates() {
        if (this.allStates == null)
        	this.allStates = new HashMap<String, LinkedList<CarState>>();
        
		return this.allStates;
	}

    public CarPosition getLatestPosition(String botName) {
        LinkedList<CarPosition> positions = getAllPositions().get(botName);
        
        if (positions != null) {
            return positions.getLast();
        }
        
        return null;
    }

    public CarState getLatestState(String botName) {
        LinkedList<CarState> states = getAllStates().get(botName);
        
        if (states != null) {
            return states.getLast();
        }
        
        return null;
    }
    
    public boolean isRaceStarted() {
        return raceStarted;
    }

    public void setRaceStarted(boolean raceStarted) {
        this.raceStarted = raceStarted;
        
        if (this.raceStarted)
        	this.currentlyCrashed = false;
    }
    
    public void updateCarPosition(CarPosition position)  {
       
        String botName = position.id.name;

        if (!getAllPositions().containsKey(botName)) {
        	getAllPositions().put(botName, new LinkedList<CarPosition>());
        }
        
        getAllPositions().get(botName).add(position);
    }
    
    public void updateCarState(CarState state)  {

        if (!getAllStates().containsKey(botName)) {
        	getAllStates().put(botName, new LinkedList<CarState>());
        }
        
        getAllStates().get(botName).add(state);
    }
    
    public TrackPiece currentPiece(CarPosition carPos) {
        return piece(carPos.currentIndex());
    }

    public TrackPiece nextPiece(CarPosition carPos) {
        return pieceAhead(carPos, 1);
    }

    public TrackPiece piece(int index) {
        return race.track.pieceAt(index);
    }

    public TrackPiece pieceAhead(CarPosition carPos, int positionsAhead) {
        return race.track.pieceAhead(carPos.currentIndex(), positionsAhead);
    }

    public TrackLane getCurrentTrackLane(CarPosition carPos) {
        Lane currentLane = carPos.piecePosition.lane;
        TrackLane trackLane = null;

        for (int i = 0; i < race.track.lanes.length; i++) {
            trackLane = race.track.lanes[i];
            if (trackLane.index == currentLane.endLaneIndex) {
                break;
            }
        }

        return trackLane;
    }
    
    public String getBotName() {
        return this.botName;
    }

    public class TrackPieceAhead {

        int index;
        TrackPiece piece;
        int countFromCurrent;
        double distanceFromCurrent;

        public TrackPieceAhead(int index, TrackPiece trackPiece, int countFromCurrent, double distanceFromCurrent) {
            this.index = index;
            this.piece = trackPiece;
            this.countFromCurrent = countFromCurrent;
            this.distanceFromCurrent = distanceFromCurrent;
        }

        public int getIndex() {
            return index;
        }

        public TrackPiece getPiece() {
            return piece;
        }

        public int getCountFromCurrent() {
            return countFromCurrent;
        }

        public double getDistanceFromCurrent() {
            return distanceFromCurrent;
        }

    }

    public TrackPieceAhead getNextTurn(CarPosition carPos) {
        return getNextTurn(carPos.currentIndex());
    }

    public TrackPieceAhead getNextTurn(int startIndex) {

        TrackPiece piece = null;

        int index = race.track.nextIndex(startIndex);
        while (true) {
            piece = race.track.pieceAt(index);

            if (piece.isBend()) {
                break;
            }

            if (index == startIndex) // just safety
            {
                return null;
            }

            index = race.track.nextIndex(index);
        }

        int nextBendPieceIndex = race.track.indexOf(piece);
        int countFromCurrent = race.track.countBetween(startIndex, nextBendPieceIndex);
        double distanceFromCurrent = race.track.distanceBetween(startIndex, nextBendPieceIndex);

        return new TrackPieceAhead(nextBendPieceIndex, piece, countFromCurrent, distanceFromCurrent);
    }

    public TrackPieceAhead getNextSwitch(int startIndex) {

        TrackPiece piece = null;

        int index = race.track.nextIndex(startIndex);
        while (true) {
            piece = race.track.pieceAt(index);

            if (piece.hasSwitch) {
                break;
            }

            if (index == startIndex) // just safety
            {
                return null;
            }

            index = race.track.nextIndex(index);
        }

        int nextBendPieceIndex = race.track.indexOf(piece);
        int countFromCurrent = race.track.countBetween(startIndex, nextBendPieceIndex);
        double distanceFromCurrent = race.track.distanceBetween(startIndex, nextBendPieceIndex);

        return new TrackPieceAhead(nextBendPieceIndex, piece, countFromCurrent, distanceFromCurrent);
    }

    public TrackPieceAhead getLastSwitchBefore(CarPosition carPos, TrackPieceAhead trackPieceAhead) {
        return getLastSwitchBefore(carPos.currentIndex(), trackPieceAhead);
    }

    public TrackPieceAhead getLastSwitchBefore(int pieceIndex, TrackPieceAhead trackPieceAhead) {
        int switchPieceIndex = -1;
        boolean switchPieceBetween = false;

        int aheadPieceIndex = trackPieceAhead.index;
        for (int i = trackPieceAhead.countFromCurrent; i > 1; i--) {

            if (race.track.previousPiece(aheadPieceIndex).hasSwitch) {
                switchPieceBetween = true;
                switchPieceIndex = i - 1;
                break;
            }

            aheadPieceIndex--;

            if (aheadPieceIndex < 0) {
                aheadPieceIndex = race.track.pieces.length;
            }
        }

        if (switchPieceBetween) {
            int countFromCurrent = race.track.countBetween(pieceIndex, aheadPieceIndex);
            double distanceFromCurrent = race.track.distanceBetween(pieceIndex, aheadPieceIndex);

            return new TrackPieceAhead(switchPieceIndex, piece(switchPieceIndex), countFromCurrent, distanceFromCurrent);
        } else {
            return null;
        }
    }

    public double distanceFromStart(CarPosition carPos) {
        double distanceFromStart = race.track.distanceFromStart(carPos.currentIndex());
        return distanceFromStart + carPos.piecePosition.inPieceDistance;
    }

	public void crash() {
		currentlyCrashed = true;

		CarState state = getLatestState(botName);
		state.speed = 0;
		state.slipAngle = 0;
		state.slipAngleIncrease = 0;
		state.centripetalForce = 0;
		state.throttle = 0.5;
		
		getAllStates().get(botName).removeLast();
		getAllStates().get(botName).add(state);

		Waypoint nextWaypoint = getWaypoints().getNextWaypoint(getLatestState(botName).pointOnTrack);
		nextWaypoint.approximatedTargetSpeed -= 10;
        getWaypoints().waypointMap.put(nextWaypoint.pointOnTrack, nextWaypoint);

		Waypoint previousWaypoint = getWaypoints().getPreviousWaypoint(getLatestState(botName).pointOnTrack);
        for (int i = 5; i > 0; i--) {
            previousWaypoint.approximatedTargetSpeed -= i * 5;
            getWaypoints().waypointMap.put(previousWaypoint.pointOnTrack, previousWaypoint);			
            
    		previousWaypoint = getWaypoints().getPreviousWaypoint(previousWaypoint.pointOnTrack);
		}

		getWaypoints().approximateDecelerationZone(previousWaypoint, true);
	}

	public void spawn() {
		currentlyCrashed = false;
	}
    
    public boolean isCurrentlyCrashed() {
		return currentlyCrashed;
	}

	public void lapFinish() {
		System.out.println(getWaypoints().toString());
		System.out.println(getSpeedChangeStates().toString());
	}

	public void gameEnd() {
		
	}
    
    public boolean isTurboAvailable() {
        return this.turbo != null;
    }

    public void setTurbo(Turbo turbo) {
        this.turbo = turbo;
    }
    
    public Turbo peekTurbo() {
        return this.turbo;
    }
    
    public Turbo useTurbo() {
        
        if (this.turbo == null) return null;
        
        Turbo t = new Turbo();
        t.turboDurationMilliseconds = turbo.turboDurationMilliseconds;
        t.turboDurationTicks = turbo.turboDurationTicks;
        t.turboFactor = turbo.turboFactor;
        
        this.turbo = null;
        
        return t;
    }
}
