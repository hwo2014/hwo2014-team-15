package noobbot.analyzer.message;

public class DriveAction {

	public enum Type {
		THROTTLE,
		SWITCH, 
		TURBO
	}
	
	public final Type type;
	public final Object content;

	
	public DriveAction(Type type, Object content) {
		super();
		this.type = type;
		this.content = content;
	}


	public static DriveAction throttleMessage(Double content) {
		return new DriveAction(Type.THROTTLE, content);
	}

	public static DriveAction switchMessage(String content) {
		return new DriveAction(Type.SWITCH, content);
	}

	public static DriveAction turboMessage(String content) {
		return new DriveAction(Type.TURBO, content);
	}
	
}
