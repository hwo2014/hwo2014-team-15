package noobbot.analyzer.message;

import noobbot.util.PathToDirectionsConverter;

public class PaceNote {

    public PathToDirectionsConverter.Command command;
    
    public boolean turboAvailable;

    @Override
    public String toString() {
        return "PaceNote{" + "command=" + command + ", turboAvailable=" + turboAvailable + '}';
    }
    
    
    
}
