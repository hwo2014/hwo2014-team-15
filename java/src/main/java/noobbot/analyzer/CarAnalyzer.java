package noobbot.analyzer;

import noobbot.analyzer.state.CarState;
import noobbot.objects.CarPosition;
import noobbot.objects.Race;
import noobbot.objects.TrackPiece;


public class CarAnalyzer {
    
	private RaceContainer raceContainer;
	private Calculator calculator;
	
	public CarAnalyzer(RaceContainer raceContainer) {
		this.raceContainer = raceContainer;
		this.calculator = new Calculator(raceContainer);
	}

	public Calculator getCalculator() {
		return calculator;
	}

    public Race getRace() {
    	return this.raceContainer.getRace();
    }

	public enum TurningPhaze {
		STRAIGHT,
		BRAKING_ZONE,
		TURN_IN,
		MID_TURN,
		TRACK_OUT
	}
	
	public CarState initialCarState() {
        CarState state = new CarState();
        state.pointOnTrack = 0;		// speed calculation: place (point on track)
        state.pointInTime = 0;		// speed calculation: time (point in time)
        state.turningPhaze = TurningPhaze.STRAIGHT;
        
        state.speed = 0;
        state.slipAngle = 0;				// start standing still
        state.throttle = 1.0;				// start throttle
        
        return state;
	}
	
	public TurningPhaze getTurningPhaze(CarPosition carPos, CarState state) {
		TurningPhaze currentPhaze;
		
		TrackPiece currentPiece = raceContainer.currentPiece(carPos);
		TrackPiece nextPiece = raceContainer.nextPiece(carPos);
		TrackPiece twoAheadPiece = raceContainer.pieceAhead(carPos, 2);
		
		double currentTrackPieceAngle = Math.abs(currentPiece.angle);
		double nextTrackPieceAngle = Math.abs(nextPiece.angle);		
		boolean turningInTheSameDirection = (currentPiece.isLeft() && nextPiece.isLeft() || currentPiece.isRight() && nextPiece.isRight());
		
		boolean brakingZone = (currentPiece.isStraight() && nextPiece.isBend()) 
				|| (currentPiece.isStraight() && nextPiece.isStraight() && twoAheadPiece.isBend() 
						&& state.speed > raceContainer.getSpeedChangeStates().observedAverageCornerEntrySpeed);
		
		boolean turnIn = !turningInTheSameDirection || (currentTrackPieceAngle == 0 && nextTrackPieceAngle > 0);
		boolean midTurn = turningInTheSameDirection;
		boolean trackOut = !turningInTheSameDirection || (currentTrackPieceAngle > 0 && nextTrackPieceAngle == 0);

		// TODO refining -- move to separate method
		// boolean opens = turningInTheSameDirection && currentTrackPieceAngle > nextTrackPieceAngle;
		// boolean tightens = turningInTheSameDirection && currentTrackPieceAngle < nextTrackPieceAngle;
		
		
		if (brakingZone)
			currentPhaze = TurningPhaze.BRAKING_ZONE;
		else if (turnIn)
			currentPhaze = TurningPhaze.TURN_IN;
		else if (midTurn)
			currentPhaze = TurningPhaze.MID_TURN;
		else if (trackOut)
			currentPhaze = TurningPhaze.TRACK_OUT;
		else
			currentPhaze = TurningPhaze.STRAIGHT;
		
		return currentPhaze;
	}

	
	public class SpeedParams {
		public double lastPosition;
		public double currentPosition;
		
		@Override
		public String toString() {
			return "SpeedParams [lastPosition=" + lastPosition + ", currentPosition="
					+ currentPosition + "]";
		}

	}

	
	public CarState calculateCarState(CarState lastCarState, CarPosition carPos) {
        CarState state = new CarState();

        SpeedParams speedParams = new SpeedParams();
        speedParams.lastPosition = lastCarState.pointOnTrack;
        speedParams.currentPosition = raceContainer.distanceFromStart(carPos);

        state.pointOnTrack = speedParams.currentPosition;
        state.turningPhaze = getTurningPhaze(carPos, state);

        state.distance = calculator.calculateTravelledDistance(speedParams);
        state.time = calculator.calculateElapsedtime(speedParams);
        state.speed = calculator.calculateSpeed(speedParams);
                
        state.centripetalForce = calculator.calculateCentripetalForce(state.speed, getRace().track.pieceAt(carPos.currentIndex()).radius);
        
        state.acceleration = calculator.calculateAcceleration(lastCarState.speed, state.speed, state.time);
        state.deceleration = calculator.calculateDeceleration(lastCarState.speed, state.speed, state.time);
        state.accelerationDistance = state.acceleration > 0 ? lastCarState.accelerationDistance + state.distance : 0;
        state.decelerationDistance = state.deceleration > 0 ? lastCarState.decelerationDistance + state.distance : 0;
        state.accelerationTime = state.acceleration > 0 ? lastCarState.accelerationTime + state.time : 0;
        state.decelerationTime = state.deceleration > 0 ? lastCarState.decelerationTime + state.time : 0;
        state.cumulativeAcceleration = state.acceleration > 0 ? state.accelerationDistance / state.accelerationTime * 1000 : 0;
        state.cumulativeDeceleration = state.deceleration > 0 ? state.decelerationDistance / state.decelerationTime * 1000 : 0;
        
        double slipAngle = calculator.calculateAbsSlipAngle(carPos);
        state.slipAngle = slipAngle;
        state.slipAngleIncrease = lastCarState.slipAngle < slipAngle ? slipAngle - lastCarState.slipAngle : 0;
        state.slipAngleDecrease = lastCarState.slipAngle > slipAngle ? lastCarState.slipAngle - slipAngle : 0;
        state.slipAngleIncreaseTotal = state.slipAngleIncrease > 0 ? lastCarState.slipAngleIncrease + state.slipAngleIncrease : 0;
        state.slipAngleDecreaseTotal = state.slipAngleDecrease > 0 ? lastCarState.slipAngleDecrease + state.slipAngleDecrease : 0;

        state.throttle = lastCarState.throttle;
        
		return state;
	}
	
	public CarState appendNewThrottle(CarState lastCarState, CarState state, double throttle) {
		state.throttle = throttle;
		state.throttleIncrease = lastCarState.throttle < throttle ? throttle - lastCarState.throttle : 0;
		state.throttleDecrease = lastCarState.throttle > throttle ? lastCarState.throttle - throttle : 0;
		state.throttleIncreaseTotal = state.throttleIncrease > 0 ? lastCarState.throttleIncrease + state.throttleIncrease : 0;
		state.throttleDecreaseTotal = state.throttleDecrease > 0 ? lastCarState.throttleDecrease + state.throttleDecrease : 0;
	
		return state;
	}
	
}
