package noobbot.analyzer.waypoints;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.TreeMap;

import noobbot.Configuration;
import noobbot.analyzer.Calculator;
import noobbot.analyzer.RaceContainer;
import noobbot.analyzer.state.CarState;
import noobbot.analyzer.state.SpeedChangeStates;
import noobbot.objects.TrackPiece;

public class Waypoints {
	private RaceContainer raceContainer;
	private Calculator calculator;
	
	public Waypoints(RaceContainer raceContainer) {
		this.raceContainer = raceContainer;
		this.calculator = new Calculator(raceContainer);
	}
		
	// points on track (key is distance from track start, value contains speed info at that point)
	public TreeMap<Double, Waypoint> waypointMap;

	
	// first approximate waypoints for whole track
	public Waypoints initializeWaypoints() {

		// already initialized during qualifying session
		if (waypointMap != null)
			return this;

		waypointMap = new TreeMap<Double, Waypoint>();
		
		initializePerTrackPiece();
		
		approximateAccelerationZones();
		approximateDecelerationZones();
		
		return this;
	}	
	
	private void initializePerTrackPiece() {
		int waypointsPerTrackPiece = 2;
		TrackPiece[] pieces = raceContainer.getRace().track.pieces;
		
		for (int i = 0; i < pieces.length; i++) {
			TrackPiece piece = pieces[i];
			double pieceDistanceFromStart = raceContainer.getRace().track.distanceFromStart(i);
			double nextWaypointDistance = piece.length / waypointsPerTrackPiece;
			
			for (int j = 0; j < waypointsPerTrackPiece; j++) {
				double pointOnTrack = pieceDistanceFromStart + (j * nextWaypointDistance);
				Waypoint waypoint = approximateInitialWaypoint(piece, pointOnTrack, i, j);
				setWaypoint(pointOnTrack, waypoint);
				
				System.out.println("INITIAL WAYPOINT: " + waypoint);
			}			
		}
	}
	
	public void approximateAccelerationZones() {
		Iterator<Double> iterator = waypointMap.navigableKeySet().iterator();		
		List<Waypoint> trackOuts = new ArrayList<>();
		while (iterator.hasNext()) {
			Double key = (Double) iterator.next();
			Waypoint wp = waypointMap.get(key);

			if (wp.isTrackOut)
				trackOuts.add(wp);
		}		

		for (Waypoint wp : trackOuts) {
			Waypoint wpCurrent = wp;
			Waypoint wpNext = getNextWaypoint(wpCurrent.pointOnTrack);

			Waypoint wpAfterTrackOut = wpNext;
			int straightPieceCount = 1;
			
			double acceleration = raceContainer.getSpeedChangeStates().getAcceleration(wp.approximatedTargetSpeed);
			double accelerationDistance = raceContainer.getSpeedChangeStates().getAccelerationDistance();
			
			while (wpNext.cornerAngle == 0) {
				double nextWaypointDistance = calculator.calculateDistance(wpCurrent.pointOnTrack, wpNext.pointOnTrack);			
				double speedIncrease = Math.abs((nextWaypointDistance / accelerationDistance) * acceleration);
//				System.out.println("speedIncrease: " + speedIncrease);
				if (speedIncrease < wp.approximatedTargetSpeed * 0.5)
					wpNext.approximatedTargetSpeed = wpNext.approximatedTargetSpeed + speedIncrease;
				waypointMap.put(wpNext.pointOnTrack, wpNext);
				
				wpCurrent = wpNext;
				wpNext = getNextWaypoint(wpCurrent.pointOnTrack);
				straightPieceCount++;
			}
			
			if (straightPieceCount > 6)
				wpAfterTrackOut.isTurboStart = true;
		}
			
	}
	
	public void approximateDecelerationZones() {
		Iterator<Double> iterator = waypointMap.navigableKeySet().iterator();
		List<Waypoint> cornerEntries = new ArrayList<>();
		while (iterator.hasNext()) {
			Double key = (Double) iterator.next();
			Waypoint wp = waypointMap.get(key);

			if (wp.isCornerEntry)
				cornerEntries.add(wp);
		}

		for (Waypoint wp : cornerEntries) {
			if (wp.cornerAngle == 45 && wp.isCornerEntry && getNextWaypoint(wp.pieceIndex).cornerAngle > 30)
				approximateDecelerationZone(wp, true);
			else
				approximateDecelerationZone(wp, false);
		}

	}
	
	public void approximateDecelerationZone(Waypoint wp, boolean crashed) {
		Waypoint wpCurrent = wp;
		Waypoint wpPrev = getPreviousWaypoint(wpCurrent.pointOnTrack);

		double deceleration = raceContainer.getSpeedChangeStates().getDeceleration(wpCurrent.approximatedTargetSpeed);
		double decelerationDistance = raceContainer.getSpeedChangeStates().getDecelerationDistance();
		double requiredSpeedDecrease = wpPrev.approximatedTargetSpeed - wp.approximatedTargetSpeed;
		
		if (crashed)
			requiredSpeedDecrease += 10;
				
		while (requiredSpeedDecrease > 0) {
			double prevWaypointDistance = calculator.calculateDistance(wpPrev.pointOnTrack, wp.pointOnTrack);			
			double speedDecrease = Math.abs((prevWaypointDistance / decelerationDistance) * deceleration);
//			System.out.println("speedDecrease: " + speedDecrease);
			if (speedDecrease < wp.approximatedTargetSpeed * 0.5)
				wpPrev.approximatedTargetSpeed = wpPrev.approximatedTargetSpeed - speedDecrease;
			waypointMap.put(wpPrev.pointOnTrack, wpPrev);
			
			wpCurrent = wpPrev;
			wpPrev = getNextWaypoint(wpCurrent.pointOnTrack);
			requiredSpeedDecrease -= speedDecrease;
		}
	}
	
	public Waypoint approximateInitialWaypoint(TrackPiece piece, Double pointOnTrack, int pieceIndex, int waypointIndexOnPiece) {
		Waypoint waypoint = new Waypoint();
		
		waypoint.pointOnTrack = pointOnTrack;	
                waypoint.pieceIndex = pieceIndex;
		waypoint.isCornerEntry = waypointIndexOnPiece == 0 && raceContainer.getRace().track.isCornerEntry(piece);
		waypoint.isMidturn = raceContainer.getRace().track.isMidturn(piece);
		waypoint.isTrackOut = waypointIndexOnPiece == 0 && raceContainer.getRace().track.isTrackOut(piece);
		double absAngle = Math.abs(piece.angle);
		waypoint.cornerAngle = absAngle;
		
		waypoint.approximatedTargetSpeed = calculator.approximateTargetSpeed(piece);
		waypoint.approximatedTargetSlipAngle = Configuration.maxAllowedSlipAngle * (absAngle > 0 ? 0.9 : 0.1);
		waypoint.approximatedTargetCentripetalForce = Configuration.maxAllowedCentripetalForce *(absAngle > 0 ? 0.9 : 0.1);

		
		return waypoint;
	}
	
	// adjust waypoint according to observations (CarState at some point near the Waypoint)
	public void recalculateWaypoint(Waypoint waypoint, CarState carState, SpeedChangeStates speedChangeStates) {
		
		if (waypoint == null || carState == null)
			return;
		
		// observation is too far away from waypoint location
		double observationdistance = Math.abs(calculator.calculateDistance(waypoint.pointOnTrack, carState.pointOnTrack));
		if (observationdistance > 25)
			return;
		
		waypoint.observedSpeed = carState.speed;
		waypoint.observedSlipAngle = carState.slipAngle;
		waypoint.observedCentripetalForce = carState.centripetalForce;
		
		// improve target speed when the observed value is somehow sane (and not way high from crash for example)
		if (waypoint.observedSpeed > waypoint.approximatedTargetSpeed 
				&& Math.abs(waypoint.observedSpeed - waypoint.approximatedTargetSpeed) < Configuration.maxApproximatedTargetSpeed / 2)
			waypoint.approximatedTargetSpeed = waypoint.observedSpeed;
		else 
			waypoint.approximatedTargetSpeed = calculator.average(waypoint.approximatedTargetSpeed, waypoint.observedSpeed);
		
		// when not slipping enough, increase waypoint target speed to push the limits
		if (waypoint.observedSlipAngle < 0.5 && !waypoint.isCornerEntry && !getNextWaypoint(waypoint.pointOnTrack).isCornerEntry) {
			waypoint.approximatedTargetSpeed += 5;
			approximateDecelerationZone(waypoint, false);
		}
		else if (waypoint.observedSlipAngle < Configuration.maxAllowedSlipAngle * 0.4 && !waypoint.isCornerEntry) {
			waypoint.approximatedTargetSpeed += 3;
			waypoint.approximatedTargetSpeed = calculator.average(waypoint.approximatedTargetSpeed, waypoint.observedSpeed);
		}
		else if (waypoint.observedSlipAngle < Configuration.maxAllowedSlipAngle * 0.6) {
			waypoint.approximatedTargetSpeed += 2;
			waypoint.approximatedTargetSpeed = calculator.average(waypoint.approximatedTargetSpeed, waypoint.observedSpeed);
		}
		else if (waypoint.observedSlipAngle > Configuration.maxAllowedSlipAngle * 0.8) {
			waypoint.approximatedTargetSpeed = waypoint.approximatedTargetSpeed -= 1;
		}
		else if (waypoint.observedSlipAngle > Configuration.maxAllowedSlipAngle) {
			waypoint.approximatedTargetSpeed = waypoint.approximatedTargetSpeed -= 2;
			
			// way too fast entry
			Waypoint previousWaypoint = getPreviousWaypoint(waypoint.pointOnTrack);
			previousWaypoint.approximatedTargetSpeed = previousWaypoint.approximatedTargetSpeed -= 3;
			setWaypoint(previousWaypoint.pointOnTrack, previousWaypoint);
		}
		
		// TODO SpeedChangeStates is not used yet
		
		setWaypoint(waypoint.pointOnTrack, waypoint);
	}
	
	private void setWaypoint(Double pointOnTrack, Waypoint newValues) {	
		waypointMap.put(pointOnTrack, newValues);
	}
	
	public Waypoint getNextWaypoint(Double pointOnTrack) {
		Waypoint waypoint = waypointMap.get(getNextKey(pointOnTrack));
		
		return waypoint;
	}

	public Waypoint getPreviousWaypoint(Double pointOnTrack) {
		Waypoint waypoint = waypointMap.get(getPreviousKey(pointOnTrack));
		
		return waypoint;
	}
	
	private Double getNextKey(Double pointOnTrack) {
		// get the nearest larger pointOnTrack -- handle lap change by returning smallest
		Double key = waypointMap.higherKey(pointOnTrack);
		
		if (key == null)
			key = waypointMap.firstKey();
		
		return key;
	}
	
	private Double getPreviousKey(Double pointOnTrack) {
		// get the nearest smaller pointOnTrack -- handle lap change by returning largest
		Double key = waypointMap.lowerKey(pointOnTrack);
		
		if (key == null)
			key = waypointMap.lastKey();
		
		return key;
	}

	@Override
	public String toString() {
		StringBuffer buffer = new StringBuffer();
		
		Iterator<Double> iterator = waypointMap.navigableKeySet().iterator();
		while (iterator.hasNext()) {
			Double key = (Double) iterator.next();
			Waypoint wp = waypointMap.get(key);
			buffer.append(wp.pointOnTrack + " approximated: " + wp.approximatedTargetSpeed + " observed: " + wp.observedSpeed + (wp.isCornerEntry ? " entry to " + wp.cornerAngle : "") + "\n");
		}
		
		return "Waypoints\n" + buffer.toString() + "\n";
	}
	
	
	
}
