package noobbot.analyzer.waypoints;

public class Waypoint {

	// distance from track start
	public double pointOnTrack;

        public double pieceIndex;
        
	public boolean isCornerEntry;
	public boolean isMidturn;
	public boolean isTrackOut;
	public boolean isTurboStart;
	public double cornerAngle;
	
	// approximations of how to drive at this way point
	public double approximatedTargetSpeed;
	public double approximatedTargetSlipAngle;
	public double approximatedTargetCentripetalForce;

	// observations of how we were able to drive at this way point
	public double observedSpeed;
	public double observedSlipAngle;
	public double observedCentripetalForce;

	
	
	@Override
	public String toString() {
		return "Waypoint [pointOnTrack=" + pointOnTrack + ", isCornerEntry="
				+ isCornerEntry + ", cornerAngle=" + cornerAngle
				+ ", approximatedTargetSpeed=" + approximatedTargetSpeed
				+ ", approximatedTargetSlipAngle="
				+ approximatedTargetSlipAngle
				+ ", approximatedTargetCentripetalForce="
				+ approximatedTargetCentripetalForce + ", observedSpeed="
				+ observedSpeed + ", observedSlipAngle=" + observedSlipAngle
				+ ", observedCentripetalForce=" + observedCentripetalForce
				+ "]";
	}


}
