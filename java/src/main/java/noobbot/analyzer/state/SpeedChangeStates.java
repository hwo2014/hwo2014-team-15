package noobbot.analyzer.state;

import noobbot.Configuration;
import noobbot.objects.TrackPiece;


public class SpeedChangeStates {
	
	double expectedAcceleration = 2;
	double expectedAccelerationDistance = 4;
	double observedAcceleration = 0;
	double observedAccelerationDistance = 0;
	double accelerationObservationCount = 0;

	double expectedDeceleration = 2;
	double expectedDecelerationDistance = 6;
	double observedDeceleration = 0;
	double observedDecelerationDistance = 0;
	double decelerationObservationCount = 0;

	public double observedMaxSpeed = 0;
	public double observedAverageSpeed = 0;
	double observationCount = 0;	// tick count
	public double observedAverageCornerEntrySpeed = 0;
	double cornerEntryObservationCount = 0;
	
	// TODO now: construct a new-throttle-effect-on-current-speed table/map
	// and a throttle chooser method that takes the requested speed difference as param

	

	public void updateWith(CarState state, TrackPiece piece) {
		++observationCount;
		
		this.observedMaxSpeed = Math.max(this.observedMaxSpeed, state.speed);
		this.observedAverageSpeed = (Math.max(1, this.observedAverageSpeed) * observationCount + state.speed) / observationCount;
		
		if (piece.angle > 0)
			this.observedAverageCornerEntrySpeed = (Math.max(1, this.observedAverageCornerEntrySpeed) * cornerEntryObservationCount + state.speed) / ++cornerEntryObservationCount;

		// update acceleration when full throttle
		if (state.acceleration > 0.1 && state.acceleration < (this.expectedAcceleration * 2)) {		// with observation sanity check
			this.observedAcceleration = (this.observedAcceleration * accelerationObservationCount + state.acceleration) / ++accelerationObservationCount;
			this.observedAccelerationDistance = (this.observedAccelerationDistance * accelerationObservationCount + state.distance) / ++accelerationObservationCount;
		}

		// update deceleration when no throttle
		if (state.deceleration > 0.1 && state.deceleration < (this.expectedDeceleration * 2)) {		// with observation sanity check
			this.observedDeceleration = (this.observedDeceleration * decelerationObservationCount + state.deceleration) / ++decelerationObservationCount;
			this.observedDecelerationDistance = (this.observedDecelerationDistance * decelerationObservationCount + state.distance) / ++decelerationObservationCount;
		}		
		
	}

	public double calculateLinearInterpolationMultiplier(double value, double averageValue) {
		double lowerBound = averageValue * (1.0 + Configuration.interpolationFactor);
		double upperBound = averageValue * (1.0 - Configuration.interpolationFactor);		

		double multiplier = value - lowerBound / upperBound - lowerBound;
		
		return multiplier;
	}
	
	public double getAcceleration(double approximatedTargetSpeed) {
		if (this.observedAcceleration == 0)
			return this.expectedAcceleration;

		return this.observedAcceleration;
	}

	public double getDeceleration(double approximatedTargetSpeed) {
		if (this.observedDeceleration == 0)
			return this.expectedDeceleration;
		
		double multiplier = calculateLinearInterpolationMultiplier(approximatedTargetSpeed, observedAverageSpeed);
		double inverseMultiplier = 2.0 - multiplier;	// invert (= higher speed, lower deceleration)
		double lowerDeceleration = observedDeceleration * (1.0 - Configuration.interpolationFactor);
		double higherDeceleration = observedDeceleration * (1.0 + Configuration.interpolationFactor);		
		
		return lowerDeceleration + inverseMultiplier * (higherDeceleration - lowerDeceleration);
	}
	
	public double getAccelerationDistance() {
		if (this.observedAccelerationDistance == 0)
			return this.expectedAcceleration;
		
		return this.observedAccelerationDistance;
	}

	public double getDecelerationDistance() {
		if (this.observedDecelerationDistance == 0)
			return this.expectedDeceleration;
		
		return this.observedDecelerationDistance;
	}

	@Override
	public String toString() {
		return "SpeedChangeStates [expectedAcceleration="
				+ expectedAcceleration + ", expectedAccelerationDistance="
				+ expectedAccelerationDistance + ", observedAcceleration="
				+ observedAcceleration + ", observedAccelerationDistance="
				+ observedAccelerationDistance
				+ ", accelerationObservationCount="
				+ accelerationObservationCount + ", expectedDeceleration="
				+ expectedDeceleration + ", expectedDecelerationDistance="
				+ expectedDecelerationDistance + ", observedDeceleration="
				+ observedDeceleration + ", observedDecelerationDistance="
				+ observedDecelerationDistance
				+ ", decelerationObservationCount="
				+ decelerationObservationCount + ", observedMaxSpeed="
				+ observedMaxSpeed + ", observedAverageSpeed="
				+ observedAverageSpeed + ", averageSpeedObservationCount="
				+ observationCount
				+ ", observedAverageCornerEntrySpeed="
				+ observedAverageCornerEntrySpeed
				+ ", cornerEntryObservationCount="
				+ cornerEntryObservationCount + "]";
	}


}
