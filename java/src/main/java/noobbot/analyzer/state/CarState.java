package noobbot.analyzer.state;

import noobbot.analyzer.CarAnalyzer.TurningPhaze;

public class CarState {
	public double pointOnTrack;
	public long pointInTime;
	public TurningPhaze turningPhaze;
	public double distanceToNextWaypoint;

	public double distance;
	public long time;
	public double speed;
	public double centripetalForce;

	public double acceleration;
	public double accelerationDistance;
	public double accelerationTime;
	public double cumulativeAcceleration;

	public double deceleration;
	public double decelerationDistance;
	public double decelerationTime;
	public double cumulativeDeceleration;
	
	public double slipAngle;
	public double slipAngleIncrease;
	public double slipAngleDecrease;
	public double slipAngleIncreaseTotal;
	public double slipAngleDecreaseTotal;
	
	public double throttle;
	public double throttleIncrease;
	public double throttleDecrease;
	public double throttleIncreaseTotal;
	public double throttleDecreaseTotal;
	
	
	@Override
	public String toString() {
		return "CarState [pointOnTrack=" + pointOnTrack + ", pointInTime="
				+ pointInTime + ", turningPhaze=" + turningPhaze
				+ ", distanceToNextWaypoint=" + distanceToNextWaypoint
				+ ", speed=" + speed + ", centripetalForce=" + centripetalForce
				+ ", acceleration=" + acceleration + ", accelerationDistance="
				+ accelerationDistance + ", accelerationTime="
				+ accelerationTime + ", cumulativeAcceleration="
				+ cumulativeAcceleration + ", deceleration=" + deceleration
				+ ", decelerationDistance=" + decelerationDistance
				+ ", decelerationTime=" + decelerationTime
				+ ", cumulativeDeceleration=" + cumulativeDeceleration
				+ ", slipAngle=" + slipAngle + ", slipAngleIncrease="
				+ slipAngleIncrease + ", slipAngleDecrease="
				+ slipAngleDecrease + ", slipAngleIncreaseTotal="
				+ slipAngleIncreaseTotal + ", slipAngleDecreaseTotal="
				+ slipAngleDecreaseTotal + ", throttle=" + throttle
				+ ", throttleIncrease=" + throttleIncrease
				+ ", throttleDecrease=" + throttleDecrease
				+ ", throttleIncreaseTotal=" + throttleIncreaseTotal
				+ ", throttleDecreaseTotal=" + throttleDecreaseTotal + ", time=" + time + "]";
	}
	
	public String accelerationState() {
		return "CarState [pointOnTrack=" + pointOnTrack 
				+ ", throttle=" + throttle
				+ ", speed=" + speed 
				+ ", acceleration=" + acceleration 
				+ "]";
	}
	
	public String decelerationState() {
		return "CarState [pointOnTrack=" + pointOnTrack 
				+ ", throttle=" + throttle
				+ ", speed=" + speed 
				+ ", deceleration=" + deceleration 
				+ "]";
	}
    		
	public String sideSlipState() {
		return "CarState [pointOnTrack=" + pointOnTrack 
				+ ", throttle=" + throttle
				+ ", speed=" + speed 
				+ ", centripetalForce=" + centripetalForce
				+ ", slipAngle=" + slipAngle 
				+ "]";
	}
	
}
