package noobbot.analyzer;

import noobbot.Configuration;
import noobbot.analyzer.CarAnalyzer.SpeedParams;
import noobbot.objects.CarPosition;
import noobbot.objects.Track;
import noobbot.objects.TrackPiece;

public class Calculator {
	private RaceContainer raceContainer;

	public Calculator(RaceContainer raceContainer) {
		this.raceContainer = raceContainer;
	}
	
	public double calculateCentripetalForce(double speed, double radius) {
		if (radius == 0)
			return 0;

		double centripetalForce = Math.pow( (speed * 1000 / 3600), 2) / radius; 
		
		return centripetalForce;
	}
	
    public double approximateTargetSpeed(TrackPiece piece, int laneId) {
        double targetSpeed = 0;
        Track track = raceContainer.getRace().track;
		int pieceIndex = track.indexOf(piece);
        
        if (piece.angle == 0)
			targetSpeed = Configuration.maxApproximatedTargetSpeed;
		else {
			double totalAngle = Math.abs(track.cornerTotalAngle(pieceIndex));
			double areaCircumference = track.cornerTotalLength(pieceIndex, laneId);			
			double totalRadius = resolveTotalRadius(areaCircumference, totalAngle);
			
			for (int i = 30; i < Configuration.maxApproximatedTargetSpeed; i += 3) {
				double speed = i;
				double centripetalForce = calculateCentripetalForce(speed, totalRadius); // piece.radius);
				
				if (centripetalForce > Configuration.maxAllowedCentripetalForce) {
					break;
				}

				targetSpeed = speed;
			}
			
			// sharp turn
			if (totalAngle >= 90 && areaCircumference < 110)
				targetSpeed *= 0.7;
			// high angle turn
			else if (totalAngle >= 90 && areaCircumference < 210 
					|| track.cornerTightens(pieceIndex) > 30)
				targetSpeed *= 0.75;
			// tightens
			else if (track.cornerTightens(pieceIndex) > 15)
				targetSpeed *= 0.8;
				
		}
		
		return targetSpeed;
    }
        
	public double approximateTargetSpeed(TrackPiece piece) {
        return approximateTargetSpeed(piece, (int) raceContainer.getRace().track.getAverageLane().index);
	}
	
	public double resolveTotalRadius(double areaCircumference, double totalAngle) {
        double oneDegreeDistance = areaCircumference / totalAngle;
        double circleCircumference = 360 * oneDegreeDistance;
        double diameter = circleCircumference / Math.PI;        
        double totalRadius = diameter / 2;

        return totalRadius;
	}
	
	public double calculateAreaCircumference(double radius, double angle) {
        double diameterOnLane = 2 * radius;
        double circleCircumference = Math.PI * diameterOnLane;
        double areaCircumference = circleCircumference * (Math.abs(angle) / 360.0);
        
        return areaCircumference;
	}
	
	public double calculateTravelledDistance(SpeedParams params) {
		return calculateDistance(params.lastPosition, params.currentPosition);
	}

	public double calculateDistance(double startPointOnTrack, double endPointOnTrack) {
		// last-first piece boundary
		if (startPointOnTrack > endPointOnTrack)
			endPointOnTrack += raceContainer.getRace().track.length();
		
		return endPointOnTrack - startPointOnTrack;
	}
	
	public long calculateElapsedtime(SpeedParams params) {
		return 100; // a constant value for one tick duration
	}
	
	public double calculateSpeed(SpeedParams params) {
		double distance = calculateTravelledDistance(params);
		long time = calculateElapsedtime(params);
				
		double speed = (distance / time) * 1000;
		
		return speed;
	}

	public double calculateAcceleration(double startSpeed, double endSpeed, long time) {
		if (startSpeed > endSpeed)
			return 0;
		
		return calculateSpeedChange(startSpeed, endSpeed, time);
	}

	public double calculateDeceleration(double startSpeed, double endSpeed, long time) {
		if (startSpeed < endSpeed)
			return 0;
		
		return calculateSpeedChange(endSpeed, startSpeed, time);
	}

	public double calculateSpeedChange(double lowerSpeed, double higherSpeed, long time) {				
		double speedChange = (higherSpeed - lowerSpeed) / time * 100;

		return speedChange;
	}
	
	public double calculateAbsSlipAngle(CarPosition carPos) {
    	double slipAngle = Math.abs(carPos.angle);

    	return slipAngle;
    }
	
	public double calculatePointOnTrack(double startPointOnTrack, double addition) {
		double trackLength = raceContainer.getRace().track.length();
		double newPointOnTrack = startPointOnTrack + addition;
		
		// last-first piece boundary
		if (newPointOnTrack > trackLength) {
			newPointOnTrack -= trackLength;
		}
		
		return newPointOnTrack;
	}
	
	public double average(double... pointsOnTrack) {
		double sum = 0;
		
		for (int i = 0; i < pointsOnTrack.length; i++) {
			sum += pointsOnTrack[i];
		}
		
		return sum / pointsOnTrack.length;
	}
	
}
