/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package noobbot;

/**
 *
 * @author teturun
 */
public class TickMsgWrapper extends MsgWrapper {

    public final int gameTick;

    TickMsgWrapper(final String msgType, final Object data, final int gameTick) {
        super(msgType, data);
        this.gameTick = gameTick;
    }
    
    public TickMsgWrapper(final SendTickMsg sendMsg) {
        this(sendMsg.msgType(), sendMsg.msgData(), sendMsg.gameTick());
    }

    @Override
    public String toString() {
        return "TickMsgWrapper{" + super.toString() + "gameTick=" + gameTick + '}';
    }
    
}
