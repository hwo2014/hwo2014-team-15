/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package noobbot;

/**
 *
 * @author teturun
 */
public class Configuration {
    
    public static boolean useAnalyzer = false;
    public static boolean useLaneSwitchPlanner = false;

    public static double maxThrottle = 1.0;					// TODO could set to lower for qualifying first lap
    public static double minThrottle = 0.0;

    public static double maxAllowedSlipAngle = 35;
    public static double maxAllowedSlipAngleIncrease = 2.3;

    public static double maxAllowedCentripetalForce = 4.8;
	public static double maxApproximatedTargetSpeed = 100;

	public static double interpolationFactor = 0.15;

}
