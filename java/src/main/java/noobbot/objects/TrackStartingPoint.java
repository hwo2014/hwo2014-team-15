/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package noobbot.objects;

public class TrackStartingPoint {
    public Position position;
    public double angle;

    @Override
    public String toString() {
        return "StartingPoint{" + "position=" + position + ", angle=" + angle + '}';
    }

}
