/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package noobbot.objects;

import com.google.gson.annotations.SerializedName;


public class TrackPiece {
    public double length;
    @SerializedName("switch") public boolean hasSwitch;
    public double radius;
    public double angle;

    public BendDirection getBendDirection() {
        if (angle < 0.0) return BendDirection.LEFT;
        if (angle > 0.0) return BendDirection.RIGHT;
        
        return BendDirection.NONE;
    }
    
    public boolean isStraight() {
    	return getBendDirection() == BendDirection.NONE;
    }

    public boolean isBend() {
    	return isLeft() || isRight();
    }

    public boolean isLeft() {
    	return getBendDirection() == BendDirection.LEFT;
    }

    public boolean isRight() {
    	return getBendDirection() == BendDirection.RIGHT;
    }

    public boolean isSameAs(BendDirection bendDirection) {
    	return getBendDirection() == bendDirection;
    }

    public double getLengthForLane(TrackLane lane) {
        
       
        if (isStraight()) { // all lanes are equal width in a straight piece
            return length;
        }
        
        double distanceFromCenter = isLeft() ? lane.distanceFromCenter : (lane.distanceFromCenter * -1.0);
        double radiusOnLane = radius + distanceFromCenter;
        double diameterOnLane = 2 * radiusOnLane;
        double circleCircumference = Math.PI * diameterOnLane;
        double pieceAreaCircumference = circleCircumference * (Math.abs(angle) / 360.0);
        
        return pieceAreaCircumference;
    }
    
    @Override
    public String toString() {
        return "Piece{" + "length=" + length + ", hasSwitch=" + hasSwitch + ", radius=" + radius + ", angle=" + angle + '}';
    }

}
