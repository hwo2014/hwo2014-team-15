/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package noobbot.objects;

public class TrackLane {
    
    public enum PositionOnTrack {
        RIGHT_FROM_CENTER,
        LEFT_FROM_CENTER,
        CENTER
    }
    
    public double distanceFromCenter;
    public double index;

    public PositionOnTrack getPositionOnTrack() {
        
        if (distanceFromCenter < 0.0) {
            return PositionOnTrack.LEFT_FROM_CENTER;
        }
        else if (distanceFromCenter > 0.0) {
            return PositionOnTrack.RIGHT_FROM_CENTER;
        }
        else {
            return PositionOnTrack.CENTER;
        }
        
    }
    
    @Override
    public int hashCode() {
        int hash = 5;
        hash = 29 * hash + (int) (Double.doubleToLongBits(this.index) ^ (Double.doubleToLongBits(this.index) >>> 32));
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final TrackLane other = (TrackLane) obj;
        if (Double.doubleToLongBits(this.index) != Double.doubleToLongBits(other.index)) {
            return false;
        }
        return true;
    }

    
    
    @Override
    public String toString() {
        return "TrackLane{" + "distanceFromCenter=" + distanceFromCenter + ", index=" + index + '}';
    }
    
}
