/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package noobbot.objects;

public class CarPiecePosition {
    public int pieceIndex;
    public double inPieceDistance;
    public Lane lane;
    public double lap;

    @Override
    public String toString() {
        return "CarPiecePosition{" + "pieceIndex=" + pieceIndex + ", inPieceDistance=" + inPieceDistance + ", lane=" + lane + ", lap=" + lap + '}';
    }
   
}
