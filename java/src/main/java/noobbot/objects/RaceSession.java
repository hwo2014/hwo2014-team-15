/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package noobbot.objects;

public class RaceSession {
    public double laps;
    public double maxLapTimeMs;
    public boolean quickRace;

    @Override
    public String toString() {
        return "RaceSession{" + "laps=" + laps + ", maxLapTimeMs=" + maxLapTimeMs + ", quickRace=" + quickRace + '}';
    }

}
