/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package noobbot.objects;

public class Dimension {
    public double length;
    public double width;
    public double guideFlagPosition;

    @Override
    public String toString() {
        return "Dimension{" + "length=" + length + ", width=" + width + ", guideFlagPosition=" + guideFlagPosition + '}';
    }

}
