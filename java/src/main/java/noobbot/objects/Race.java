/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package noobbot.objects;

public class Race {
    public Track track;
    public Car[] cars;
    public RaceSession raceSession;

    @Override
    public String toString() {
        return "Race{" + "track=" + track + ", cars=" + cars + ", raceSession=" + raceSession + '}';
    }
    
}
