/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package noobbot.objects;

public class CarPosition {
    public Id id;
    public double angle;
    public CarPiecePosition piecePosition;

    public int currentIndex() {
    	return piecePosition.pieceIndex;
    }
    
    public Lane currentLane() {
        return piecePosition.lane;
    }
    
    @Override
    public String toString() {
        return "CarPositions{" + "id=" + id + ", angle=" + angle + ", piecePosition=" + piecePosition + '}';
    }
   
}
