package noobbot.objects;

import java.util.HashMap;
import java.util.Map;

public class Track {
    public String id;
    public String name;
    public TrackPiece[] pieces;
    public TrackLane[] lanes;
    public TrackStartingPoint startingPoint;

    private double trackLength = 0;
    private Map<TrackPiece, Integer> indexOfMap;
    public double cornersAverageAngle;
    public double cornersAverageLength;
    
    
    public TrackPiece pieceAt(int index) {
    	return pieces[index];
    }
    
    public TrackPiece nextPiece(int currentIndex) {
    	return pieces[nextIndex(currentIndex)];
    }

    public TrackPiece previousPiece(int currentIndex) {
    	return pieces[previousIndex(currentIndex)];
    }

	public int indexOf(TrackPiece piece) {
    	if (indexOfMap == null) {
    		indexOfMap = new HashMap<TrackPiece, Integer>(pieces.length);
    		
	    	for (int i = 0; i < pieces.length; i++) {
	    		indexOfMap.put(pieces[i], i);
			}
    	}
    	
		return indexOfMap.get(piece);
	}
    
    public double length() {
    	if (trackLength == 0) {
	    	for (int i = 0; i < pieces.length; i++) {
	    		trackLength += getPieceLength(pieces[i]);
			}
    	}
    	
    	return trackLength;
    }


    public double getPieceLength(TrackPiece piece) {
    	return piece.getLengthForLane(getAverageLane());
    }
    
    public int getAverageLaneIndex() {
    	return lanes.length / 2;
    }
        
    public TrackLane getAverageLane() {
        return lanes[getAverageLaneIndex()];
    }
    
    public int previousIndex(int index) {
		index--;
		
		// rotate when necessary
		if (index < 0) index = pieces.length - 1;
		
    	return index;
    }
    
    public int nextIndex(int index) {
		index++;
		
		// rotate when necessary
		if (index >= pieces.length) index = 0;
		
    	return index;
    }
    
    public int countBetween(int startPieceIndex, int endPieceIndex) {
    	int countBetween = 0;
    	
    	// add all before end piece
    	int i = nextIndex(startPieceIndex);		
    	while (i != endPieceIndex) {    		
    		countBetween += 1;    		    		
    		i = nextIndex(i);
    	}

    	return countBetween;
    }
    
    public double distanceBetween(int startPieceIndex, int endPieceIndex) {
    	double distanceBetween = 0;
    	
    	// add all before end piece
    	int i = nextIndex(startPieceIndex);		
    	while (i != endPieceIndex) {    		
    		distanceBetween += getPieceLength(pieces[i]);    		    		
    		i = nextIndex(i);
    	}

    	return distanceBetween;
    }

    public double distanceFromStart(int endPieceIndex) {
    	int startPieceIndex = previousIndex(0);
    	return distanceBetween(startPieceIndex, endPieceIndex);
    }
    
	public TrackPiece pieceAhead(int startPieceIndex, int positionsAhead) {    	
    	int targetIndex = startPieceIndex;	
    	int count = 0;
    	while (count < positionsAhead) {    		
    		targetIndex = nextIndex(targetIndex);    		
    		count++;
    	}
    	
		return pieces[targetIndex];
	}
    
    public int pieceCount() {
    	return pieces.length;
    }
    
    @Override
    public String toString() {
        return "Track{" + "id=" + id + ", name=" + name + ", pieces=" + pieces + ", lanes=" + lanes + ", startingPoint=" + startingPoint + '}';
    }
 
    
    public static void main(String[] args) {
    	Track track = new Track();
    	track.pieces = new TrackPiece[3];
    	
    	track.pieces[0] = new TrackPiece();
    	track.pieces[1] = new TrackPiece();
    	track.pieces[2] = new TrackPiece();
    	
    	track.pieces[0].length = 1;
    	track.pieces[1].length = 10;
    	track.pieces[2].length = 100;
    	
    	System.out.println("length " + track.length());

    	System.out.println("countBetween 1 0: " + track.countBetween(1, 0));
    	System.out.println("countBetween 0 2: " + track.countBetween(0, 2));
    	System.out.println("countBetween 2 1: " + track.countBetween(2, 1));

    	System.out.println("lengthBetween 1 0: " + track.distanceBetween(1, 0));
    	System.out.println("lengthBetween 0 2: " + track.distanceBetween(0, 2));
    	System.out.println("lengthBetween 2 1: " + track.distanceBetween(2, 1));

    	System.out.println("previousPiece 0 " + track.previousPiece(0));
    	System.out.println("previousPiece 1 " + track.previousPiece(1));

    	System.out.println("nextPiece 1 " + track.nextPiece(1));
    	System.out.println("nextPiece 2 " + track.nextPiece(2));

    	System.out.println("pieceAhead 2 2 " + track.pieceAhead(2, 2));
    	System.out.println("pieceAhead 0 2 " + track.pieceAhead(0, 2));

	}

	public boolean isCornerEntry(TrackPiece piece) {
		TrackPiece previousPiece = previousPiece(indexOf(piece));
		boolean turningInTheSameDirection = previousPiece.isSameAs(piece.getBendDirection());

		// entering from straight or bend to the other direction
		if (!turningInTheSameDirection || (previousPiece.isStraight() && piece.isBend())) {			
			return true;
		}
		
		return false;
	}
	
	public boolean isMidturn(TrackPiece piece) {
		TrackPiece previousPiece = previousPiece(indexOf(piece));
		boolean turningInTheSameDirection = previousPiece.isSameAs(piece.getBendDirection());

		if (piece.isBend() && turningInTheSameDirection) {			
			return true;
		}
		
		return false;
	}

	public boolean isTrackOut(TrackPiece piece) {
		TrackPiece previousPiece = previousPiece(indexOf(piece));
		boolean turningInTheSameDirection = previousPiece.isSameAs(piece.getBendDirection());

		if (!turningInTheSameDirection || (previousPiece.isBend() && piece.isStraight())) {
			return true;
		}
		
		return false;
	}

    public double cornerTotalAngle(int startPieceIndex) {
    	double totalAngle = pieces[startPieceIndex].angle;

    	int i = nextIndex(startPieceIndex);		
    	while (isMidturn(pieces[i])) {    		
    		totalAngle += pieces[i].angle;
    		i = nextIndex(i);
    	}

    	return Math.abs(totalAngle);
    }

    public double cornerTightens(int startPieceIndex) {
    	double currentAngle = Math.abs(pieces[startPieceIndex].angle);
    	double nextAngle = Math.abs(pieces[nextIndex(startPieceIndex)].angle);

    	return nextAngle > currentAngle ? nextAngle - currentAngle : 0;
    }

    public double cornerOpens(int startPieceIndex) {
    	double currentAngle = Math.abs(pieces[startPieceIndex].angle);
    	double nextAngle = Math.abs(pieces[nextIndex(startPieceIndex)].angle);

    	return nextAngle < currentAngle ? currentAngle - nextAngle : 0;
    }
    
    public double cornerTotalLength(int startPieceIndex, int laneId) {
    	double totalLength = pieces[startPieceIndex].getLengthForLane(lanes[laneId]); //getPieceLength(pieces[startPieceIndex]);

    	int i = nextIndex(startPieceIndex);		
    	while (isMidturn(pieces[i])) {    		
    		totalLength += pieces[i].getLengthForLane(lanes[laneId]);
    		i = nextIndex(i);
    	}

    	return totalLength;
    }    
        
    public double cornersAverageAngle() {
    	if (cornersAverageAngle == 0) {
    		double pieceAngles = 0;
	    	for (int i = 0; i < pieces.length; i++) {
	    		pieceAngles += pieces[i].angle;
			}
    		cornersAverageAngle = pieceAngles / pieces.length;
    	}
    	
    	return cornersAverageAngle;
    }    
    
	public double cornersAverageLength() {
		if (cornersAverageLength == 0) {
			double pieceLengths = 0;
	    	for (int i = 0; i < pieces.length; i++) {
	    		pieceLengths += getPieceLength(pieces[i]);
			}
	    	cornersAverageLength = pieceLengths / pieces.length;
		}
		
		return cornersAverageLength;
	}

}
