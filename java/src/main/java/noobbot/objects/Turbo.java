/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package noobbot.objects;

/**
 *
 * @author teturun
 */
public class Turbo {
    public double turboDurationMilliseconds;
    public double turboDurationTicks;
    public double turboFactor;

    @Override
    public String toString() {
        return "Turbo{" + "turboDurationMilliseconds=" + turboDurationMilliseconds + ", turboDurationTicks=" + turboDurationTicks + ", turboFactor=" + turboFactor + '}';
    }
    
    
}
