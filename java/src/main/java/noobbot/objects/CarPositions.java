/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package noobbot.objects;

import java.util.List;

/**
 *
 * @author teturun
 */
public class CarPositions {
    
    public final List<CarPosition> positions;
    public final int gameTick;
    
    
    public CarPositions(List<CarPosition> positions, int gameTick) {
        this.positions = positions;
        this.gameTick = gameTick;
    }
    
}
