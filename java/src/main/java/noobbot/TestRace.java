/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package noobbot;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author teturun
 */
public class TestRace {
    
    public static final String TEST_SERVER_LOAD_BALANCER = "testserver.helloworldopen.com";
    public static final String TEST_SERVER_R1 = "senna.helloworldopen.com";
    public static final String TEST_SERVER_R2 = "hakkinen.helloworldopen.com";
    public static final String TEST_SERVER_R3 = "webber.helloworldopen.com";
    public static final String TEST_SERVER_CONSTANT_PHYSICS = "prost.helloworldopen.com";
    
    public static final String TEST_TRACK_KEIMOLA = "keimola";
    public static final String TEST_TRACK_GERMANY = "germany";
    public static final String TEST_TRACK_EASTER_EGG = "usa";
    public static final String TEST_TRACK_FRANCE = "france";
    public static final String TEST_TRACK_ELAEINTARHA = "elaeintarha";
    public static final String TEST_TRACK_ITALY = "imola";
    public static final String TEST_TRACK_ENGLAND = "england";
    public static final String TEST_TRACK_JAPAN = "suzuka";
    
    
    public static void main(String[] args) {
        final String host = TEST_SERVER_CONSTANT_PHYSICS;
        final int port = 8091;
        final String botName = "louhibot";
        final String botKey = "+K3Fyy+w83BAng";
        
        final String trackName = TEST_TRACK_ENGLAND;
        final String password = "1234";
        final int nrOfCars = 1;
        
        for (int i = 0; i < nrOfCars; i++) {

            final int nr = i;
                new Thread() {
                  @Override
                  public void run() {
                      System.out.println("Running..." + nr);
                      try {
                          Main m = new Main(host, port, botName +"-" +nr, botKey, trackName, password, nrOfCars, nr > 0);
                      } catch (IOException ex) {
                          Logger.getLogger(TestRace.class.getName()).log(Level.SEVERE, null, ex);
                      }
                  }
                }.start();
            try {
                Thread.sleep(500);
            } catch (InterruptedException ex) {
                Logger.getLogger(TestRace.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
    }
    
}
