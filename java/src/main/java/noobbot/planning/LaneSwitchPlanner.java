/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package noobbot.planning;

import java.util.HashMap;
import noobbot.util.PathToDirectionsConverter;

/**
 *
 * @author teturun
 */
public interface LaneSwitchPlanner {

    public PathToDirectionsConverter.Command getCurrentDirection(String botName);
    public HashMap<Integer, PathToDirectionsConverter.Command> getDirections(String botName);
    public void recalculate(String botName);

    public void lockPlanner(boolean lock);
    
}
