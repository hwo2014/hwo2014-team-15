/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package noobbot.planning;

import com.louhigames.graph.algorithms.pathfinding.PathFindingAlgorithm;
import com.louhigames.graph.algorithms.pathfinding.PathFindingCostComparator;
import com.louhigames.graph.algorithms.pathfinding.SearchResult;
import com.louhigames.graph.algorithms.pathfinding.astar.AStarAlgorithm;
import com.louhigames.graph.algorithms.pathfinding.astar.ShortestLengthAStarCostComparator;
import java.util.List;
import java.util.concurrent.Callable;
import noobbot.graph.Graph;
import noobbot.graph.GraphMoveCostCalculator;
import noobbot.graph.Node;

/**
 *
 * @author avilja
 */
public class CalculatePathCall implements Callable<List<Node>> {

    private final Graph graph;
    private final Node start;
    private final Node goal;
    
    private final PathFindingAlgorithm algorithm;
    private final GraphMoveCostCalculator calculator;
    private final PathFindingCostComparator costComparator;
    
    public CalculatePathCall(Graph graph, Node start, Node goal) {
        this.graph = graph;
        this.start = start;
        this.goal = goal;
        this.algorithm = new AStarAlgorithm();
        this.calculator = new GraphMoveCostCalculator();
        this.costComparator = new ShortestLengthAStarCostComparator();
    }
    
    @Override
    public List<Node> call() throws Exception {
        
        SearchResult rs = algorithm.findPath(start, goal, graph, calculator, costComparator);
        
        return rs.getPath();
    }
    
}
