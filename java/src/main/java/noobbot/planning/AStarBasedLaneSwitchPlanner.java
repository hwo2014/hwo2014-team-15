/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package noobbot.planning;

import com.louhigames.graph.algorithms.pathfinding.PathFindingAlgorithm;
import com.louhigames.graph.algorithms.pathfinding.PathFindingCostComparator;
import com.louhigames.graph.algorithms.pathfinding.SearchResult;
import com.louhigames.graph.algorithms.pathfinding.astar.AStarAlgorithm;
import com.louhigames.graph.algorithms.pathfinding.astar.FastestPathAStarCostComparator;
import com.louhigames.graph.algorithms.pathfinding.astar.ShortestLengthAStarCostComparator;
import java.util.HashMap;
import java.util.LinkedList;
import noobbot.analyzer.RaceContainer;
import noobbot.graph.Graph;
import noobbot.graph.GraphMoveCostCalculator;
import noobbot.graph.Node;
import noobbot.objects.CarPosition;
import noobbot.util.PathToDirectionsConverter;
import noobbot.util.PathToDirectionsConverter.Command;

/**
 *
 * @author teturun
 */
public class AStarBasedLaneSwitchPlanner implements LaneSwitchPlanner {

    private RaceContainer raceContainer;
    private HashMap<Integer, PathToDirectionsConverter.Command> directions;
    private boolean locked;
    private final PathCalculationService pathCalculationService = new PathCalculationService();
    private final PathToDirectionsConverter pathToDirectionsConverter = new PathToDirectionsConverter();
    
    public AStarBasedLaneSwitchPlanner(){
    }

    public AStarBasedLaneSwitchPlanner(RaceContainer raceContainer) {
        this.raceContainer = raceContainer;
    }
    
    @Override
    public PathToDirectionsConverter.Command getCurrentDirection(String botName) {
       
       // System.out.println("=========================================Directions for " + botName);
        
        if(raceContainer == null) {
            return Command.NO_ACTION;
        }
        
        if (locked) return PathToDirectionsConverter.Command.NO_ACTION;
        
        LinkedList<CarPosition> positions = raceContainer.getAllPositions().get(botName);

        if (directions == null || directions.isEmpty()) {
            directions = getDirections(botName);
            //for (int i = 0; i < raceContainer.getRace().track.pieceCount(); i++) {
            //    System.out.println(i + " cmd=" + directions.get(i));
           // }
        }

        PathToDirectionsConverter.Command cmd = PathToDirectionsConverter.Command.NO_ACTION;

        if (directions != null && positions != null && directions.containsKey(positions.getLast().currentIndex())) {
            cmd = directions.get(positions.getLast().currentIndex());
        }

        return cmd;
    }

    @Override
    public HashMap<Integer, PathToDirectionsConverter.Command> getDirections(String botName) {
        
        LinkedList<CarPosition> positions = raceContainer.getAllPositions().get(botName);
        CarPosition pos = positions.getLast();
        
        if (pos == null) return null;
        
        Graph g = raceContainer.getRaceGraph();
        g.refreshWeights();
        Node startNode = g.getNode(pos.currentIndex(), pos.currentLane().endLaneIndex);
       // Node goalNode = g.getPreviousNode(startNode);
        
       // System.out.println(g);
        
        if (startNode == null) return null;
        
        PathFindingAlgorithm pathFinding = new AStarAlgorithm();
        SearchResult bestResult = null;
        PathFindingCostComparator costComparator = g.getWeightCalculator().getCostComparator();
       
        for (Node goalNode : g.getPreviousNodes(startNode)) {
            SearchResult rs = pathFinding.findPath(startNode, goalNode, g, new GraphMoveCostCalculator(), costComparator);
            
            if (bestResult == null || costComparator.compare(bestResult.getPathValue(), rs.getPathValue()) > 0) {
                bestResult = rs;
            }
            
        }
        
        
        HashMap<Integer, Command> directions = PathToDirectionsConverter.getDirections(bestResult.getPath());
        
        // TODO Not tested.
        //HashMap<Integer, Command> directions = pathToDirectionsConverter.getDirections(pathCalculationService.getLatestPath());
        
        return directions;
    }

    @Override
    public void lockPlanner(boolean lock) {
        this.locked = lock;
    }

    @Override
    public void recalculate(String botName) {
        directions = getDirections(botName);
    }
    
    
    
}
