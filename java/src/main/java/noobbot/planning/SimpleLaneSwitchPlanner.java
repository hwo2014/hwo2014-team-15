/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package noobbot.planning;

import java.util.HashMap;
import java.util.LinkedList;
import noobbot.analyzer.RaceContainer;
import noobbot.objects.BendDirection;
import noobbot.objects.CarPosition;
import noobbot.objects.TrackLane;
import noobbot.objects.TrackPiece;
import noobbot.util.PathToDirectionsConverter;

/**
 *
 * @author teturun
 */
public class SimpleLaneSwitchPlanner implements LaneSwitchPlanner {

    private RaceContainer raceContainer;
    private HashMap<Integer, PathToDirectionsConverter.Command> directions;
    
    private boolean locked;

    public SimpleLaneSwitchPlanner(RaceContainer raceContainer) {
        this.raceContainer = raceContainer;
    }
    
    @Override
    public PathToDirectionsConverter.Command getCurrentDirection(String botName) {

        if (locked) return PathToDirectionsConverter.Command.NO_ACTION;
        
        LinkedList<CarPosition> positions = raceContainer.getAllPositions().get(botName);

        if (directions == null) {
            directions = getDirections(botName);
            for (int i = 0; i < raceContainer.getRace().track.pieceCount(); i++) {
                System.out.println(i + " cmd=" + directions.get(i));
            }
        }

        PathToDirectionsConverter.Command cmd = PathToDirectionsConverter.Command.NO_ACTION;

        if (directions.containsKey(positions.getLast().currentIndex())) {
            cmd = directions.get(positions.getLast().currentIndex());
        }

        return cmd;
    }

    public HashMap<Integer, PathToDirectionsConverter.Command> getDirections(String botName) {

        HashMap<Integer, PathToDirectionsConverter.Command> commands = new HashMap<>();
        LinkedList<CarPosition> positions = raceContainer.getAllPositions().get(botName);
        CarPosition carPos = positions.getLast();
        TrackLane currentLane = raceContainer.getCurrentTrackLane(carPos);

        for (int i = carPos.piecePosition.pieceIndex; i < raceContainer.getRace().track.pieceCount(); i++) {

            System.out.println("[pieceId=" + i + "]: START");
            // get next bend
            RaceContainer.TrackPieceAhead nextBendPiece = raceContainer.getNextTurn(i);

            TrackPiece currentPiece = raceContainer.piece(i);

            System.out.println("[pieceId=" + i + "]: NEXTBENDPIECE " + nextBendPiece.getIndex());

            System.out.println("[pieceId=" + i + "]: HAS SWITCH " + currentPiece.hasSwitch);

            PathToDirectionsConverter.Command cmd = PathToDirectionsConverter.Command.NO_ACTION;
            if (currentPiece.hasSwitch) {

                if (nextBendPiece.getPiece() != null && nextBendPiece.getPiece().getBendDirection() == BendDirection.LEFT && currentLane.distanceFromCenter >= 0.0) { // left turn
                    cmd = PathToDirectionsConverter.Command.TURN_LEFT;
                } else if (nextBendPiece.getPiece() != null && nextBendPiece.getPiece().getBendDirection() == BendDirection.RIGHT && currentLane.distanceFromCenter <= 0.0) { // right turn
                    cmd = PathToDirectionsConverter.Command.TURN_RIGHT;
                } else {
                    cmd = PathToDirectionsConverter.Command.NO_ACTION;
                }
            } else {
                cmd = PathToDirectionsConverter.Command.NO_ACTION;
            }

            if (cmd == PathToDirectionsConverter.Command.TURN_LEFT) {

                if (currentLane.index - 1 >= 0) {
                    currentLane = raceContainer.getRace().track.lanes[(int) currentLane.index - 1];
                }

            } else if (cmd == PathToDirectionsConverter.Command.TURN_RIGHT) {

                if (currentLane.index + 1 < raceContainer.getRace().track.lanes.length) {
                    currentLane = raceContainer.getRace().track.lanes[(int) currentLane.index + 1];
                }

            }

            System.out.println("[pieceId=" + i + "]: COMMAND " + cmd);

            int pieceIndex = i;
            if (cmd != PathToDirectionsConverter.Command.NO_ACTION) {
                pieceIndex = Math.max(0, pieceIndex - 1);
            }
            commands.put(pieceIndex, cmd);
            commands.put(i, PathToDirectionsConverter.Command.NO_ACTION);

        }

        return commands;

    }

    @Override
    public void lockPlanner(boolean lock) {
        locked = lock;
    }

    @Override
    public void recalculate(String botName) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
