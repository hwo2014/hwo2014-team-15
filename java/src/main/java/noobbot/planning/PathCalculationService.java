/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package noobbot.planning;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.logging.Level;
import java.util.logging.Logger;
import noobbot.graph.Graph;
import noobbot.graph.Node;

/**
 *
 * @author avilja
 */
public class PathCalculationService {

    private final ExecutorService executor;
    private Future<List<Node>> future;
    private List<Node> latestPath;

    public PathCalculationService() {

        executor = Executors.newSingleThreadExecutor();
        latestPath = new LinkedList<>();

    }

    public void tick(Graph graph, Node start, Node goal) { // TODO Provide data.

        // Update parameters with new data.
        if (future != null) {

            if (future.isDone()) {
                try {
                    latestPath = future.get();
                    future = null;
                } catch (InterruptedException ex) {
                    Logger.getLogger(PathCalculationService.class.getName()).log(Level.SEVERE, null, ex);
                } catch (ExecutionException ex) {
                    Logger.getLogger(PathCalculationService.class.getName()).log(Level.SEVERE, null, ex);
                }
            } else if (future.isCancelled()) {

                future = null;

            }
        }

        if (future == null) {
            future = executor.submit(new CalculatePathCall(graph, start, goal));
        }
    }

    public List<Node> getLatestPath() {
        return latestPath;
    }
}
