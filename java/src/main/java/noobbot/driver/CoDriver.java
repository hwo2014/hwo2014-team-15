package noobbot.driver;

import java.util.HashMap;
import java.util.LinkedList;
import noobbot.analyzer.RaceContainer;
import noobbot.analyzer.message.PaceNote;
import noobbot.events.CarEnteredTrackPieceEvent;
import noobbot.events.CarFinishedLapEvent;
import noobbot.events.CarSwitchedLaneEvent;
import noobbot.events.TrackListener;
import noobbot.objects.CarPosition;
import noobbot.planning.AStarBasedLaneSwitchPlanner;
import noobbot.util.PathToDirectionsConverter.Command;

public class CoDriver implements TrackListener {

    private RaceContainer raceContainer;
    private AStarBasedLaneSwitchPlanner switchPlanner;

    private Command nextSwitchCommand;
    private boolean switchNow;

    public CoDriver(RaceContainer raceContainer) {
        this.raceContainer = raceContainer;
        this.switchPlanner = new AStarBasedLaneSwitchPlanner(raceContainer);

        this.nextSwitchCommand = Command.NO_ACTION;
        this.switchNow = false;
    }

	// TODO later: observe the opponents (from here up to the parts that affect next throttle/switch choice)
    // -- "care opponent ahead" (opponent ahead on same lane and gap closing, crash warning)
    // -- "care opponent inside" (opponent on inside lane when about to turn, switch collision warning)
    // -- "care opponent behind" (opponent behind and gap closing, try to block overtaking or dodge rear bumping ??)
	// TODO later: plan overtaking choices
    
    public PaceNote tellPaceNote() {
        PaceNote paceNote = new PaceNote();

        if (this.switchNow && this.nextSwitchCommand != Command.NO_ACTION) {
            paceNote.command = this.nextSwitchCommand;
            this.switchNow = false;
        } 

        paceNote.turboAvailable = raceContainer.isTurboAvailable();

        return paceNote;
    }

    @Override
    public void carEnteredTrackPiece(CarEnteredTrackPieceEvent e) {
        this.nextSwitchCommand = switchPlanner.getCurrentDirection(raceContainer.getBotName());

        HashMap<String, LinkedList<CarPosition>> positions = raceContainer.getAllPositions();
        
        CarPosition ourPosition = positions.get(raceContainer.getBotName()).getLast();
        
        for(String car : positions.keySet()) {
            if(!car.equals(raceContainer.getBotName())) {
                CarPosition enemyPosition = positions.get(car).getLast();
                if ( ((enemyPosition.currentIndex() == ourPosition.currentIndex() && enemyPosition.piecePosition.inPieceDistance > ourPosition.piecePosition.inPieceDistance) || 
            		  enemyPosition.currentIndex() == ourPosition.currentIndex() + 1)	
                    && enemyPosition.currentLane().startLaneIndex == ourPosition.currentLane().startLaneIndex) {
                   
                	int averageLaneIndex = raceContainer.getRace().track.getAverageLaneIndex();
                	double ourLaneIndex = ourPosition.currentLane().startLaneIndex;
                	
                	// stay close to center lanes
            		if(ourLaneIndex <= averageLaneIndex) {
                       this.nextSwitchCommand = Command.TURN_LEFT; 
                       System.out.println("Going for an overtake!");
                    } else {
                        this.nextSwitchCommand = Command.TURN_RIGHT;
                        System.out.println("Going for an overtake!");
                    }
                }
            }
        }
        
        
        if (e.getTo().hasSwitch) {
            this.switchNow = true;
        }

    }

    @Override
    public void carSwitchedLanes(CarSwitchedLaneEvent e) {
    	if (e != null && e.getCar().id.name.equals(raceContainer.getBotName())) {
	        this.switchNow = false;
	        this.nextSwitchCommand = Command.NO_ACTION;
    	}
    }

    @Override
    public void carFinishedLap(CarFinishedLapEvent e) {
        if (e != null && e.getCar().id.name.equals(raceContainer.getBotName())) {
            //System.out.println("--------------------------------------------------------------RECALCULATE");
            switchPlanner.recalculate(raceContainer.getBotName());
        }
    }

}
