package noobbot.driver;

import noobbot.Configuration;
import noobbot.analyzer.message.DriveAction;
import noobbot.analyzer.message.PaceNote;
import noobbot.analyzer.state.CarState;
import noobbot.analyzer.waypoints.Waypoint;
import noobbot.util.PathToDirectionsConverter;

public class Driver {
    boolean sendSwitchMessage = false;
    boolean sendTurboMessage = false;

    public DriveAction drive(PaceNote paceNote, CarState carState, Waypoint nextWaypoint) {

    	// turbo
    	if (paceNote.turboAvailable && nextWaypoint.isTurboStart && carState.slipAngle > Configuration.maxAllowedSlipAngle / 3)
    		sendTurboMessage = true;

    	// switch
        String nextSwitch = null;
        if (paceNote.command == PathToDirectionsConverter.Command.TURN_LEFT) {
            nextSwitch = "Left";
            sendSwitchMessage = true;
        } else if (paceNote.command == PathToDirectionsConverter.Command.TURN_RIGHT) {
            nextSwitch = "Right";
            sendSwitchMessage = true;
        }

        // throttle
        double chosenThottle = adjustThrottleTowardsTargetSpeed(carState, nextWaypoint.approximatedTargetSpeed,
                carState.distanceToNextWaypoint, carState.throttle);

        if (sendSwitchMessage) {
        	sendSwitchMessage = false;
            return DriveAction.switchMessage(nextSwitch);
        } else if (sendTurboMessage) {
        	sendTurboMessage = false;
            return DriveAction.turboMessage("CHAAAAARGE");
        } else {
            return DriveAction.throttleMessage(chosenThottle);
        }

    }

    public double adjustThrottleTowardsTargetSpeed(CarState state, double targetSpeed, double distanceToTargetSpeed, double lastThrottle) {
        double difference = state.speed - targetSpeed;
        double throttle = lastThrottle;

        boolean slidingTooMuchSA = state.slipAngle > Configuration.maxAllowedSlipAngle;
        boolean slidingTooMuchSAI = state.slipAngleIncrease > Configuration.maxAllowedSlipAngleIncrease;
        boolean slidingTooMuchCF = state.centripetalForce > Configuration.maxAllowedCentripetalForce;
        boolean slidingTooMuch = slidingTooMuchSAI || slidingTooMuchCF || slidingTooMuchSA;


        if (state.speed > targetSpeed) {
            throttle = 0.0;
        } // try to straighten with throttle
        else if (slidingTooMuchSA && !slidingTooMuchSAI) {
        	throttle = Math.min(0.2, lastThrottle + 0.1);
        } // go faster
        else if (difference < 0 && !slidingTooMuch) {
            throttle = lastThrottle + 0.1;
        } // slow down 
        else if (difference > 0 || slidingTooMuch) {
            throttle = lastThrottle / 2;
            // throttle = lastThrottle - 0.1;
        } // hold when close enough
        else if (Math.abs(difference) < 3) {
            throttle = lastThrottle;
        }

        // limit throttle to range edge values
        throttle = Math.max(Configuration.minThrottle, throttle);
        throttle = Math.min(Configuration.maxThrottle, throttle);

        return throttle;
    }

}
