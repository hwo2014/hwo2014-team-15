package com.louhigames.graph.algorithms.pathfinding;

import noobbot.graph.Node;



public interface PathFindingObject {
	
	public float getCostForMove(Node fromNode, Node toNode);
	public float getExtraCostForMove(Node fromNode, Node toNode);
	
}
