/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.louhigames.graph.algorithms.pathfinding.astar;

import com.louhigames.graph.algorithms.pathfinding.PathFindingCostComparator;

/**
 *
 * @author teturun
 */
public class ShortestLengthAStarCostComparator implements PathFindingCostComparator {

    @Override
    public int compare(double cost1, double cost2) {        
        return Double.compare(cost1, cost2);
    }
    
}
