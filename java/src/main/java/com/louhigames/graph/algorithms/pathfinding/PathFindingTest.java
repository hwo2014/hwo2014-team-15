/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.louhigames.graph.algorithms.pathfinding;

import com.louhigames.graph.algorithms.pathfinding.astar.AStarAlgorithm;
import com.louhigames.graph.algorithms.pathfinding.astar.ShortestLengthAStarCostComparator;
import java.util.HashMap;
import noobbot.graph.Graph;
import noobbot.graph.GraphMoveCostCalculator;
import noobbot.graph.Node;
import noobbot.graph.GraphFactory;
import noobbot.util.PathToDirectionsConverter;
import noobbot.util.PathToDirectionsConverter.Command;
/**
 *
 * @author teturun
 */
public class PathFindingTest {

    public static void main(String... args) {
    	/*
        Graph g = GraphFactory.getTestGraph(GraphFactory.TestTrack.KEIMOLA);
       // //System.out.println("-------");
        System.out.println(g);
        //System.out.println("--------");
        Node startNode = g.getNodes().get(0);
        Node goalNode = g.getPreviousNode(startNode);

        //System.out.println(g);
        System.out.println("START=" + startNode + "\nGOAL=" + goalNode);

        PathFindingAlgorithm pathFinding = new AStarAlgorithm();
        SearchResult rs = pathFinding.findPath(startNode, goalNode, g, new GraphMoveCostCalculator(), new ShortestLengthAStarCostComparator());
        
        PathToDirectionsConverter u = new PathToDirectionsConverter();
        HashMap<Integer, Command> directions = u.getDirections(rs.getPath());
        
        
         System.out.println("Results:");
         for (Node n : rs.getPath()) {
         Command cmd = directions.get(n.getTrackPieceId());
         // System.out.println(n + " command=" + cmd + " ");
         }
        
         System.out.println("Used candidates:");
         for (Node n : rs.getUsedCandidates()) {
         System.out.println(n);
         }
        
         System.out.println("Unused candidates:");
         for (Node n : rs.getUnUsedCandidates()) {
         System.out.println(n);
         }*/

    }

}
