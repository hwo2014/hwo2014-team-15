/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.louhigames.graph.algorithms.pathfinding.astar;

import com.louhigames.graph.algorithms.pathfinding.PathFindingCostComparator;

/**
 *
 * @author teturun
 */
public class FastestPathAStarCostComparator implements PathFindingCostComparator {

    @Override
    public int compare(double cost1, double cost2) {
    	// note, inverse comparison
        return Double.compare(cost2, cost1);
    }
    
}
