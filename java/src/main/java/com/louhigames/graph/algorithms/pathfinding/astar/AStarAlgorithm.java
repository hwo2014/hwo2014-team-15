package com.louhigames.graph.algorithms.pathfinding.astar;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.PriorityQueue;

import com.louhigames.graph.algorithms.pathfinding.PathFindingAlgorithm;
import com.louhigames.graph.algorithms.pathfinding.PathFindingCostComparator;
import com.louhigames.graph.algorithms.pathfinding.PathFindingObject;
import com.louhigames.graph.algorithms.pathfinding.SearchResult;
import com.louhigames.graph.algorithms.pathfinding.astar.heuristics.HCostHeuristic;
import noobbot.graph.Graph;
import noobbot.graph.Node;

/**
 * Class for A* path finding algorithm implementation. For more information
 * about A*, see<br>
 * http://en.wikipedia.org/wiki/A*_search_algorithm<br>
 * http://www.policyalmanac.org/games/aStarTutorial.htm<br>
 *
 */
public class AStarAlgorithm implements PathFindingAlgorithm {

    private final AStarCost aStarCost;
    
    
    private Graph graph;
    private PathFindingObject pathFindingObject;

    public AStarAlgorithm() {
        this.aStarCost = new AStarCost();
    }

    public PathFindingCostComparator getCostComparator() {
        return aStarCost.getCostComparator();
    }

    public void setCostComparator(PathFindingCostComparator costComparator) {
        aStarCost.setCostComparator(costComparator);
    }
    
    /**
     * Returns cost heuristics that can be used in this A* implementation.
     *
     * @return AStarCost object containing heuristics
     */
    public AStarCost getCostHeuristics() {
        return this.aStarCost;
    }

    /**
     * Sets h cost heuristic (see some A* tutorial for more information) for
     * this A* implementation
     *
     * @param heuristic h cost heuristic
     */
    public void setHCostHeuristic(HCostHeuristic heuristic) {
        this.aStarCost.setHCostHeuristic(heuristic);
    }

    @Override
    /**
     * Overrides and implements search for A*
     */
    public SearchResult findPath(Node startPoint, Node goalPoint, Graph graph, PathFindingObject pathFindingObject, PathFindingCostComparator costComparator) {

        if (startPoint == null || goalPoint == null || graph == null) {
            return null;
        }

        this.graph = graph;
        this.pathFindingObject = pathFindingObject;
        this.aStarCost.setCostComparator(costComparator);

        List<AStarNode> path = new ArrayList<>();
        PriorityQueue<AStarNode> open = new PriorityQueue<>();
        HashSet<AStarNode> closed = new HashSet<>();
//System.out.println("Start=" + startPoint + " Goal=" + goalPoint);
        this.doSearch(startPoint, goalPoint, open, closed, path);
//System.out.println("Results:");
        List<Node> pathPoints = new ArrayList<>();
        for (int i = path.size()-1; i >= 0; i--) {
            AStarNode n = path.get(i);
          //  System.out.println(n.getOriginalNode() + " FSCORE=" + n.getFScore() + " GCOST=" + n.getGCost() + " HCOST=" + n.getHCost());
            pathPoints.add(n.getOriginalNode());
        }
//System.out.println("Used candidates:");
        List<Node> usedCandidates = new ArrayList<>();
        for (AStarNode n : closed) {
        //    System.out.println(n.getOriginalNode() + " FSCORE=" + n.getFScore() + " GCOST=" + n.getGCost() + " HCOST=" + n.getHCost());
            usedCandidates.add(n.getOriginalNode());
        }
//System.out.println("Unused candidates:");
        List<Node> unUsedCandidates = new ArrayList<>();
        for (AStarNode n : open) {
       //     System.out.println(n.getOriginalNode() + " FSCORE=" + n.getFScore() + " GCOST=" + n.getGCost() + " HCOST=" + n.getHCost());
            unUsedCandidates.add(n.getOriginalNode());
        }

        return new SearchResult(pathPoints, usedCandidates, unUsedCandidates);
    }

    /**
     * Constructs the found path.
     *
     */
    private void constructPath(AStarNode node, List<AStarNode> path, HashSet<AStarNode> closed) {
       
       // System.out.println("Contruct");
        if (node.getParent() != null) {
            //System.out.println("Contruct ADD");
            path.add(node);
            constructPath(node.getParent(), path, closed);
        } else {
            path.add(node);
        }

    }

    /**
     * Searches path between given start and goal point using map. Adds map
     * points to open and closed collections (see some A* tutorial for more
     * information) and the found path to path collection.
     *
     * @return true, if path was found and false if not
     */
    private boolean doSearch(Node start, Node goal, PriorityQueue<AStarNode> open, HashSet<AStarNode> closed, List<AStarNode> path) {

        AStarNode startNode = new AStarNode(start, null, aStarCost.getCostComparator());

        AStarNode goalNode = new AStarNode(goal, null, aStarCost.getCostComparator());

        startNode.setGCost(0f);
        startNode.setHCost(this.aStarCost.calculateHCost(startNode, goalNode, startNode, pathFindingObject));
        open.add(startNode);

        while (!open.isEmpty()) {

            AStarNode current = open.poll();
           // System.out.println("current=" + current);
            if (current == null) {
               // System.out.println("CURR NULL");
                break;
            }

            if (current.equals(goalNode)) {
                //System.out.println("EQ");
                constructPath(current, path, closed);
                break;
            } else if (current.getOriginalNode().equals(goalNode.getOriginalNode())) {
               // System.out.println("EQ2");
            }

            closed.add(current);

            addNeighbours(current, goalNode, open, closed);

        }

        return !path.isEmpty();

    }

    /**
     * Checks neighbours for give Node and adds the suitable ones to
     * PriorityQueue<AStarNode> open.
     *
     */
    private void addNeighbours(AStarNode current, AStarNode goal, PriorityQueue<AStarNode> open, HashSet<AStarNode> closed) {

        if (current == null || goal == null || open == null || closed == null || graph == null) {
            return;
        }

        List<Node> adjacentNodes = current.getOriginalNode().getNeighbors();
//System.out.println("addNeighbours for " + current);
        for (Node node : adjacentNodes) {
           // System.out.println("---- Node is " + node);
            calculateNode(node, current, goal, open, closed, graph);
        }
//System.out.println("----------------------------");
    }

    /**
     * Creates and calculates node.
     */
    private void calculateNode(Node neighbourNode, AStarNode current, AStarNode goal, PriorityQueue<AStarNode> open, HashSet<AStarNode> closed, Graph world) {

        //System.out.println("closed size=" + closed.size());
        if (closed.contains(neighbourNode)) {
            return;
        }
//System.out.println("-------------------> not in closed yet");
        AStarNode neighbour = new AStarNode(neighbourNode, current, aStarCost.getCostComparator());
        AStarNode node = null;

        // check if neighbour node is in open nodes and if it is, get it
        Iterator<AStarNode> iter = open.iterator();
        while (iter.hasNext()) {
            AStarNode n = iter.next();
            //System.out.println("n=" + n + " neighbour=" + neighbour);
            if (n.equals(neighbour)) {
               // System.out.println("FOUND MATCH!");
                node = n;
                break;
            }
        }

        //System.out.println("IS NODE NOT NULL?");
        if (node != null) {

            //System.out.println("NODE IS NOT NULL");
            // check if current node would be better parent for node
            float gCostWithNewParent = this.aStarCost.calculateGCost(node, current, pathFindingObject);
           // System.out.println("IS THIS BETTER PARENT=" + current + "GCOST=" +gCostWithNewParent + " FOR=" + node + " HAD GCOST=" + node.getGCost());
            if (aStarCost.getCostComparator().compare(gCostWithNewParent, node.getGCost()) < 0) {
                //System.out.println("FOUND BETTER PARENT=" + current + "GCOST=" +gCostWithNewParent + " FOR=" + node + " HAD GCOST=" + node.getGCost());
                node.setParent(current);
                node.setGCost(gCostWithNewParent);
                node.setHCost(this.aStarCost.calculateHCost(node, goal, current, pathFindingObject));

            }

        } else { // neighbour node not in open list => add it
           //System.out.println("NODE IS NULL");
            neighbour.setParent(current);
            neighbour.setGCost(this.aStarCost.calculateGCost(neighbour, current, pathFindingObject));
            neighbour.setHCost(this.aStarCost.calculateHCost(neighbour, goal, current, pathFindingObject));
//System.out.println("-------------- Add to OPEN: " + neighbour);
            open.add(neighbour);
        }

    }

    @Override
    public String toString() {
        return "A* algorithm";
    }

}
