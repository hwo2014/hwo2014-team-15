package com.louhigames.graph.algorithms.pathfinding.astar;

import com.louhigames.graph.algorithms.pathfinding.PathFindingCostComparator;
import noobbot.graph.Node;


public class AStarNode implements Comparable<AStarNode> {

    private PathFindingCostComparator costComparator;
    private AStarNode parent;

    private Node originalNode;

    private float gCost;
    private float hCost;

    public AStarNode(Node node, AStarNode parent, PathFindingCostComparator costComparator) {

        originalNode = node;
        this.parent = parent;
        
        this.costComparator = costComparator;
        this.gCost = 0f;
        this.hCost = 0f;
    }

    public float getGCost() {
        return gCost;
    }

    public float getHCost() {
        return hCost;
    }

    public void setGCost(float gCost) {
        this.gCost = gCost;
    }

    public void setHCost(float hCost) {
        this.hCost = hCost;
    }

    public float getFScore() {

        float fScore = this.gCost + this.hCost;

        // if cost is negative we presumably went over Integer.MAX_VALUE -> set it back to max
       // if (fScore < 0f) {
       //     fScore = Float.MAX_VALUE;
       // }

        return fScore;
    }

    public AStarNode getParent() {
        return parent;
    }

    public void setParent(AStarNode parent) {
        this.parent = parent;
    }

    @Override
    public boolean equals(Object object) {

        if (object == null) {
            return false;
        }

        if (object instanceof AStarNode) {

            return this.originalNode.equals(((AStarNode) object).getOriginalNode());

        } else if (object instanceof Node) {
            
           return originalNode.equals(object); 
            
        }
        
        return false;
    }

    @Override
    public int hashCode() {
        return originalNode.hashCode();
    }

    @Override
    public int compareTo(AStarNode object) {

        if (object == this) {
            return 0;
        }
        
        int fScoreComparison = costComparator.compare(this.getFScore(), object.getFScore());

        if (fScoreComparison != 0) return fScoreComparison;
        
        // tie breaking
        int hCostComparison = costComparator.compare(this.getHCost(), object.getHCost());
        
        return hCostComparison;

    }

    public Node getOriginalNode() {
        return originalNode;
    }

    public void setOriginalNode(Node originalNode) {
        this.originalNode = originalNode;
    }

    @Override
    public String toString() {
        return "AStarNode{" + "originalNode=" + originalNode + '}';
    }

    
    
}
