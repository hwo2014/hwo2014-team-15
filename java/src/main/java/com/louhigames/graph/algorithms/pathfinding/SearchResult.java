package com.louhigames.graph.algorithms.pathfinding;

import java.util.ArrayList;
import java.util.List;

import noobbot.graph.Node;
import noobbot.objects.CarPosition;
import noobbot.objects.TrackPiece;
import noobbot.util.PathToDirectionsConverter.Command;

public class SearchResult {

    private List<Node> path; // the found path
    private List<Node> usedCandidates; // points that were estimated and chosen
    private List<Node> unUsedCandidates; // points that were estimated but not chosen

    public SearchResult() {
        this(new ArrayList<Node>(),
                new ArrayList<Node>(),
                new ArrayList<Node>());
    }

    public SearchResult(List<Node> path, List<Node> usedCandidates, List<Node> unUsedCandidates) {
        this.path = path;
        this.usedCandidates = usedCandidates;
        this.unUsedCandidates = unUsedCandidates;
    }

    public List<Node> getPath() {
        return path;
    }

    public void setPath(List<Node> path) {
        this.path = path;
    }

    public List<Node> getUsedCandidates() {
        return usedCandidates;
    }

    public void setUsedCandidates(List<Node> usedCandidates) {
        this.usedCandidates = usedCandidates;
    }

    public List<Node> getUnUsedCandidates() {
        return unUsedCandidates;
    }

    public void setUnUsedCandidates(List<Node> unUsedCandidates) {
        this.unUsedCandidates = unUsedCandidates;
    }

    public long getPathValue() {
        long value = 0;
        for (Node node : path) {
            value += node.getWeight();
        }
        return value;
    }

    public Command getCommand(CarPosition position) {
        for (int ii = 0; ii < path.size(); ii++) {
            Node node = path.get(ii);
            if (node.getTrackPieceId() == (int) position.currentIndex()) {
                if (node.getLaneId() == (int) position.currentLane().endLaneIndex
                        || node.getLaneId() == (int) position.currentLane().startLaneIndex) {
                    return Command.NO_ACTION;
                } else {
                    if (node.getLaneId() < (int) position.currentLane().endLaneIndex) {
                        return Command.TURN_LEFT;
                    } else {
                        return Command.TURN_RIGHT;
                    }
                }

            }
        }

        return null;
    }

    public TrackPiece getNextTrackPieceOnPath(CarPosition position) {

        for (int ii = 0; ii < path.size(); ii++) {
            Node node = path.get(ii);
            if (node.getTrackPieceId() == (int) position.currentIndex()) {
                if (ii + 1 < path.size()) {
                    return path.get(ii + 1).getPiece();
                } else {
                    return path.get(0).getPiece();
                }
            }

        }
        return null;
    }

}
