package com.louhigames.graph.algorithms.pathfinding;

import noobbot.graph.Graph;
import noobbot.graph.Node;

/**
 * Interface for path finding algorithms.
 *
 * @author arttu viljakainen
 * @author teturun
 */
public interface PathFindingAlgorithm {

    public SearchResult findPath(Node start, Node goal, Graph graph, PathFindingObject pathFindingObject, PathFindingCostComparator costComparator);

}
