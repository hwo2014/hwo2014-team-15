/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.louhigames.graph.algorithms.pathfinding;

/**
 *
 * @author teturun
 */
public interface PathFindingCostComparator {
    
        /**
     * Compares two given costs with a criteria determined by the implementation.
     * 
     * @param cost1
     * @param cost2
     * @return -1, if cost1 is better (determined by some criteria) than cost2, 0 if costs are equal, 1, if cost2 is better than cost1 
     */
    public int compare(double cost1, double cost2);
    
}
