package com.louhigames.graph.algorithms.pathfinding.astar;


import com.louhigames.graph.algorithms.pathfinding.PathFindingCostComparator;
import com.louhigames.graph.algorithms.pathfinding.PathFindingObject;
import com.louhigames.graph.algorithms.pathfinding.astar.heuristics.EuclideanDistance;
import com.louhigames.graph.algorithms.pathfinding.astar.heuristics.HCostHeuristic;
import java.util.LinkedList;
import java.util.List;
import noobbot.graph.Node;

/**
 * Class for holding information and methods for calculating costs for A* moves.
 *
 * Includes methods for calculating G cost and H cost (see some A* tutorial for
 * more information).
 *
 * Also includes possibility to change and add new H cost heuristics to be used.
 *
 * @author teturun
 *
 */
public class AStarCost {

    public final static float NORMAL_MOVE = 10f;
    public final static float DIAGONAL_MOVE = 14f;

    private final List<HCostHeuristic> heuristics;
    private PathFindingCostComparator costComparator;
    private int index;

    /**
     * Constructor for AStarCost.
     */
    public AStarCost() {
        this.heuristics = new LinkedList<>();

        this.addHCostHeuristic(new EuclideanDistance());
        
        this.costComparator = new ShortestLengthAStarCostComparator();
        
        this.index = 0;
    }

    /**
     * Add new H cost heuristic.
     *
     * @param heuristic H cost heuristic
     */
    public void addHCostHeuristic(HCostHeuristic heuristic) {

        if (!this.heuristics.contains(heuristic)) {
            this.heuristics.add(heuristic);
        }

    }

    /**
     * Remove H cost heuristic.
     *
     * @param heuristic H cost heuristic
     * @return true, if heuristic was removed, false if not
     */
    public boolean removeHCostHeuristic(HCostHeuristic heuristic) {
        return this.heuristics.remove(heuristic);
    }

    /**
     * Returns all heuristics added.
     *
     * @return Vector of H cost heuristics
     */
    public List<HCostHeuristic> getHeuristics() {
        return this.heuristics;
    }

    /**
     * Returns the H cost heuristic that is used at the moment.
     *
     * @return the H cost heuristic that is used at the moment
     */
    public HCostHeuristic getHCostHeuristic() {
        if (this.index >= 0 && this.index < this.heuristics.size()) {
            return this.heuristics.get(this.index);
        } else {
            return null;
        }
    }

    /**
     * Set H cost heuristic that is to be used.
     *
     * @param heuristic H cost heuristic that is to be used
     */
    public void setHCostHeuristic(HCostHeuristic heuristic) {
        this.index = heuristics.indexOf(heuristic);

        if (this.index == -1) {
            this.heuristics.add(heuristic);
            this.setHCostHeuristic(heuristic);
        }
    }

    /**
     * Calculates G cost (see some A* tutorial for more information).
     *
     * @param node to which the G cost will be calculated
     * @param parent start Node's parent Node
     */
    public float calculateGCost(AStarNode node, AStarNode parent, PathFindingObject object) {

        Node originalNode = node != null ? node.getOriginalNode() : null;
        Node originalParentNode = parent != null ? parent.getOriginalNode() : null;

        float moveCost = object.getCostForMove(originalParentNode, originalNode);
        float extraCost = object.getExtraCostForMove(originalParentNode, originalNode);

        float cost = 0f;

        if (parent == null) {
            cost = extraCost + moveCost;
        } else {
            float gCost = parent.getGCost();
            cost = (extraCost + gCost + moveCost);
        }

        // if cost is negative we presumably went over Float.MAX_VALUE -> set it back to max
       // if (cost < 0f) {
       //     cost = Float.MAX_VALUE;
       // }

		//System.out.println("Node: " + node.getOriginalNode().getPosition().toString() + " type: " + node.getOriginalNode().getVerticalHole() +  " g cost: " + cost);
        return cost;
    }

    /**
     * Calculates the H cost (see some A* tutorial for more information)
     * according to set H cost heuristic.
     *
     * @param start Node to which the H cost will be calculated
     * @param goal Node that is the set goal for search
     * @param current
     * @param pathFindingObject
     * @return calculated H cost
     */
    public float calculateHCost(AStarNode start, AStarNode goal, AStarNode current, PathFindingObject pathFindingObject) {

        Node originalStartNode = start != null ? start.getOriginalNode() : null;
        Node originalGoalNode = goal != null ? goal.getOriginalNode() : null;
        Node originalCurrentNode = current != null ? current.getOriginalNode() : null;

        return this.getHCostHeuristic().calculateCost(originalStartNode, originalGoalNode, originalCurrentNode, pathFindingObject);
    }

    public PathFindingCostComparator getCostComparator() {
        return costComparator;
    }

    public void setCostComparator(PathFindingCostComparator costComparator) {
        this.costComparator = costComparator;
    }
    
}
