package com.louhigames.graph.algorithms.pathfinding.astar.heuristics;

import com.louhigames.graph.algorithms.pathfinding.PathFindingObject;
import noobbot.graph.Node;


public interface HCostHeuristic {

	public abstract float calculateCost(Node start, Node goal, Node current, PathFindingObject pathFindingObject);
}
