package com.louhigames.graph.algorithms.pathfinding.astar.heuristics;

import com.louhigames.graph.algorithms.pathfinding.PathFindingObject;
import noobbot.graph.Node;

public class EuclideanDistance implements HCostHeuristic {

        @Override
	public float calculateCost(Node start, Node goal, Node current, PathFindingObject pathFindingObject) {
            return 0;
		//return start.getValue() + goal.getValue() + current.getValue(); // TODO I have no idea what this should be.
	}

        @Override
	public String toString() {
		return "Euclidean distance";
	}
	
}
